package com.pkgroupmanagement.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.pkgroupmanagement.R;
import com.pkgroupmanagement.api.response.SearchComplainListResponse;
import com.pkgroupmanagement.interfaces.SelectedItemListener;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.services.complainList.ReplaceProductActivity;
import com.pkgroupmanagement.services.complainList.TestActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchComplainListAdapter extends RecyclerView.Adapter<SearchComplainListAdapter.MyViewHolder>{

    private List<SearchComplainListResponse.ComplainList> complainListData;
    SelectedItemListener itemListener;

    public SearchComplainListAdapter(List<SearchComplainListResponse.ComplainList> data, SelectedItemListener itemListener){

        this.complainListData = data;
        this.itemListener = itemListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_complain_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final SearchComplainListResponse.ComplainList comList = complainListData.get(position);
        holder.complainId.setText(comList.id);
        holder.name.setText(comList.dealer_name);
        holder.productName.setText(comList.st_product_name);
        holder.district.setText(comList.dealer_district);
        holder.barcode.setText(comList.pid);
        holder.date.setText(comList.updated_at);

        if(MyApplication.prefsHelper.getRole().equalsIgnoreCase("SRO")){
            holder.btnContinueTest.setVisibility(View.INVISIBLE);
        }
        else{
            holder.btnContinueTest.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemListener.onItemClicked(complainListData.get(position));
            }
        });

        if(comList.is_new.equals("1")){
            holder.card_view.setCardBackgroundColor(Color.WHITE);
        }
        else if(comList.is_new.equals("2")){
            holder.card_view.setCardBackgroundColor(holder.card_view.getContext().getResources().getColor(R.color.redLight));
        }
        else{
            holder.card_view.setCardBackgroundColor(holder.card_view.getContext().getResources().getColor(R.color.offWhite));
        }

        if(comList.st_decision!= null && comList.st_decision.equals("pending_rep")){
            holder.btnContinueTest.setText("Replace Product");
        }

        holder.btnContinueTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(comList.st_decision!= null && comList.st_decision.equals("pending_rep")){
                    Intent intent = new Intent(ReplaceProductActivity.getStartIntent(view.getContext()));
                    intent.putExtra("id", comList.id);
                    view.getContext().startActivity(intent);
                }
                else {
                    Intent intent = new Intent(TestActivity.getStartIntent(view.getContext()));
                    intent.putExtra("id", comList.id);
                    intent.putExtra("pid", comList.pid);
                    intent.putExtra("isNew", comList.is_new);
                    view.getContext().startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return complainListData.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.card_view) CardView card_view;
        @BindView(R.id.complainId) TextView complainId;
        @BindView(R.id.name) TextView name;
        @BindView(R.id.productName) TextView productName;
        @BindView(R.id.district) TextView district;
        @BindView(R.id.barcode) TextView barcode;
        @BindView(R.id.date) TextView date;
        @BindView(R.id.continueTest) Button btnContinueTest;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
