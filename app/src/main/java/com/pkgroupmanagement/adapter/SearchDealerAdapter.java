package com.pkgroupmanagement.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pkgroupmanagement.R;
import com.pkgroupmanagement.api.response.DealerListResponse;
import com.pkgroupmanagement.services.createComplain.CreateComplainActivity;
import com.pkgroupmanagement.services.createComplain.SearchDealerActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchDealerAdapter extends RecyclerView.Adapter<SearchDealerAdapter.MyViewHolder>{

    private List<DealerListResponse> dealerListData;
    private Context context;

    public SearchDealerAdapter(Context context, List<DealerListResponse> data){

        this.dealerListData = data;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_dealer, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        holder.name.setText(dealerListData.get(position).name);

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchDealerActivity.dealerItem = dealerListData.get(position);
                finishActivity();
            }
        });
    }

    @Override
    public int getItemCount() {
        return dealerListData.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.name) TextView name;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public void addData(List<DealerListResponse> data){
        this.dealerListData = data;
    }

    public void clearData(){
        this.dealerListData.clear();
    }


    void finishActivity(){
        SearchDealerActivity activity = (SearchDealerActivity)this.context;
        activity.finish();
    }


}
