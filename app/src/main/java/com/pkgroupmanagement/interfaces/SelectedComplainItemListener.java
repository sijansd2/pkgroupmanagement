package com.pkgroupmanagement.interfaces;

import com.pkgroupmanagement.api.response.ComplainListResponse;
import com.pkgroupmanagement.api.response.SearchComplainListResponse;

public interface SelectedComplainItemListener {

    void onItemClicked(ComplainListResponse.ComplainList item);
}
