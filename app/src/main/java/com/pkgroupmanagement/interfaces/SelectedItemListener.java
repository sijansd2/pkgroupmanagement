package com.pkgroupmanagement.interfaces;

import com.pkgroupmanagement.api.response.SearchComplainListResponse;

public interface SelectedItemListener {

    void onItemClicked(SearchComplainListResponse.ComplainList item);
}
