package com.pkgroupmanagement.splash;


import com.pkgroupmanagement.model.DataManager;
import com.pkgroupmanagement.base.BasePresenter;

/**
 * Created by macbookpro on 12/9/17.
 */

public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V> implements SplashMvpPresenter<V> {

    public SplashPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void nextActivity() {

        //getMvpView().openServiceActivity();

        if(getDataManager().getLoggedInMode()){

            getMvpView().openServiceActivity();
        }
        else{
            getMvpView().openLoginActivity();
        }
    }
}
