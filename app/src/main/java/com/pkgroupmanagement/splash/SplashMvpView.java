package com.pkgroupmanagement.splash;


import com.pkgroupmanagement.base.MvpView;

/**
 * Created by macbookpro on 12/9/17.
 */

public interface SplashMvpView extends MvpView {

    void openServiceActivity();
    void openLoginActivity();

}
