package com.pkgroupmanagement.splash;

import com.pkgroupmanagement.base.MvpPresenter;

/**
 * Created by macbookpro on 12/9/17.
 */

public interface SplashMvpPresenter<V extends SplashMvpView> extends MvpPresenter<V> {

    void nextActivity();
}
