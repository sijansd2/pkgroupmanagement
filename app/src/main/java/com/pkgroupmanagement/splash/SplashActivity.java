package com.pkgroupmanagement.splash;


import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.pkgroupmanagement.R;
import com.pkgroupmanagement.api.APIService;
import com.pkgroupmanagement.api.ApiUtils;
import com.pkgroupmanagement.api.response.VersionCheckResponse;
import com.pkgroupmanagement.main.MainActivity;
import com.pkgroupmanagement.model.DataManager;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.base.BaseActivity;
import com.pkgroupmanagement.login.LoginActivity;
import com.pkgroupmanagement.services.ServiceActivity;
import com.pkgroupmanagement.services.complainList.TestActivity;
import com.pkgroupmanagement.utils.network.Connectivity;

import java.io.PrintWriter;
import java.io.StringWriter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Created by macbookpro on 12/9/17.
 */

public class SplashActivity extends BaseActivity implements SplashMvpView {


    SplashPresenter mSplashPresenter;
    String version = "2.3.9";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        DataManager dataManager = ((MyApplication)getApplication()).getDataManager();
        mSplashPresenter = new SplashPresenter(dataManager);
        mSplashPresenter.onAttach(this);

        if(Connectivity.isConnected(this)) {
            checkVersion();
        }
        else{
            showNetworkSettingsAlert();
        }
        //mSplashPresenter.nextActivity();
    }

    @Override
    public void openServiceActivity() {

        Intent intent = new Intent(this, ServiceActivity.class);
        if(MyApplication.prefsHelper.getRole().equalsIgnoreCase("SRO")){
            intent.putExtra(ServiceActivity.KEY_CLICK_TYPE, ServiceActivity.KEY_CLICK_SRO);
        }
        else{
            intent.putExtra(ServiceActivity.KEY_CLICK_TYPE, ServiceActivity.KEY_CLICK_SERVICE_EXE);
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void openLoginActivity() {

        startActivity(LoginActivity.getStartIntent(this));
        finish();
    }

    private void checkVersion(){

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if(MyApplication.prefsHelper.getApiToken() != null)
        Log.e("aaaApiToken", MyApplication.prefsHelper.getApiToken());

        APIService apiService = new ApiUtils().getAPIService();
        apiService.checkVersion()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<VersionCheckResponse>() {
                    @Override
                    public void onNext(VersionCheckResponse versionCheckResponse) {

                        if(versionCheckResponse.message.equalsIgnoreCase("Success")){
                            MyApplication.prefsHelper.setAppVersion(versionCheckResponse.app_version);

                            if(!TextUtils.isEmpty(version)){
                                if(!version.equalsIgnoreCase(versionCheckResponse.app_version)){
                                    showAlert();
                                }
                                else{
                                    mSplashPresenter.nextActivity();
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        StringWriter errors = new StringWriter();
                        t.printStackTrace(new PrintWriter(errors));
                        Toast.makeText(SplashActivity.this, errors.toString(), Toast.LENGTH_LONG).show();

                        t.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void showAlert(){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(getResources().getString(R.string.checkVersionMessage));
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Update",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                        dialog.cancel();
                        finish();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    private void showNetworkSettingsAlert(){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(getResources().getString(R.string.check_internet_details));
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       //finish();
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity( intent);
                        finish();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
