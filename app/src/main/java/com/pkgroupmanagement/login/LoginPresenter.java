package com.pkgroupmanagement.login;

import com.pkgroupmanagement.model.DataManager;
import com.pkgroupmanagement.base.BasePresenter;

/**
 * Created by macbookpro on 12/9/17.
 */

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V>{


    public LoginPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void setGoogleSignInRequest() {
        getMvpView().onGoogleSignInRequest();
    }

    @Override
    public void setLoginMode() {
        getDataManager().setLoggedInMode();
    }

    @Override
    public void setEmailAddress(String email) {
        getDataManager().saveEmailId(email);
    }

    @Override
    public void setApiToken(String api_token) {
        getDataManager().setApiToken(api_token);
    }

    @Override
    public void getApiToken() {
        getDataManager().getApiToken();
    }

    @Override
    public String getEmailAddress() {
        return getDataManager().getEmailId();
    }

    @Override
    public void setAccessToken(String token) {
        getDataManager().saveAccessToken(token);
    }

    @Override
    public String getAccessToken() {
        return getDataManager().getAccessToken();
    }

    @Override
    public void openServiceActivity() {
        getMvpView().openServiceActivity();
    }
}
