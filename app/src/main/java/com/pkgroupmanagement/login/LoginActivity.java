package com.pkgroupmanagement.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.pkgroupmanagement.model.DataManager;
import com.pkgroupmanagement.R;
import com.pkgroupmanagement.base.BaseActivity;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.model.SharedPrefsHelper;
import com.pkgroupmanagement.services.ServiceActivity;
import com.pkgroupmanagement.utils.network.Connectivity;
import com.pkgroupmanagement.utils.network.MyOkHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by macbookpro on 12/9/17.
 */

public class LoginActivity extends BaseActivity implements LoginMvpView, View.OnClickListener {


    LoginMvpPresenter loginMvpPresenter;
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 1;
    int AUTH_REQUEST_CODE = 2;
    String TAG = "-----log tag---";
    private ProgressDialog pDialog;

    public void showProgressBar() {
        ProgressDialog();
        pDialog.show();
    }

    public void hideProgressBar() {
        if(pDialog != null) {
            pDialog.dismiss();
        }
    }
    public  void ProgressDialog()
    {
        if(pDialog == null){
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
        }

    }
    public static final String SCOPES = "https://www.googleapis.com/auth/plus.login "
            + "https://www.googleapis.com/auth/drive.file";


    public static Intent getStartIntent(Context context){
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

        Log.d(TAG, "LastSignedInAccount: "+account);

        if(account != null){

        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        DataManager dataManager = ((MyApplication)getApplication()).getDataManager();
        loginMvpPresenter = new LoginPresenter(dataManager);
        loginMvpPresenter.onAttach(this);
        loginMvpPresenter.setGoogleSignInRequest();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void openServiceActivity() {

        Intent intent = new Intent(this, ServiceActivity.class);
        if(MyApplication.prefsHelper.getRole().equalsIgnoreCase("SRO")){
            intent.putExtra(ServiceActivity.KEY_CLICK_TYPE, ServiceActivity.KEY_CLICK_SRO);
        }
        else{
            intent.putExtra(ServiceActivity.KEY_CLICK_TYPE, ServiceActivity.KEY_CLICK_SERVICE_EXE);
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void onGoogleSignInRequest() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        findViewById(R.id.sign_in_button).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                if(Connectivity.isConnected(this)){
                    signIn();
                }else {
                    showSnackBar(getResources().getString(R.string.check_internet));
                }

                break;
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

        else if(requestCode == AUTH_REQUEST_CODE){
            //new GetAccessToken().execute();
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Log.d(TAG, "email address: "+account.getEmail());
            Log.d(TAG, "account id: "+account.getId());

            if(!TextUtils.isEmpty(account.getEmail())){
                loginMvpPresenter.setEmailAddress(account.getEmail());
                new GetAccessToken().execute(account.getEmail());
            }
            // Signed in successfully, show authenticated UI.
        } catch (Exception e) {

            e.printStackTrace();
        }
    }




    public class GetAccessToken extends AsyncTask<String, Void, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
            protected String doInBackground(String... params) {
                String token = null;

                try {
                    token = GoogleAuthUtil.getToken(
                            LoginActivity.this,
                            params[0],
                            "oauth2:" + SCOPES);
                } catch (IOException transientEx) {
                    // Network or server error, try later
                    Log.e(TAG, transientEx.toString());
                } catch (UserRecoverableAuthException e) {
                    // Recover (with e.getIntent())
                    Log.e(TAG, e.toString());
                    Intent recover = e.getIntent();
                    startActivityForResult(recover, AUTH_REQUEST_CODE);
                } catch (GoogleAuthException authEx) {
                    // The call is not ever expected to succeed
                    // assuming you have already verified that
                    // Google Play services is installed.
                    Log.e(TAG, authEx.toString());
                }

                if(token != null){
                    return token;
                }else {
                    return getResources().getString(R.string.api_not_success);
                }
            }

            @Override
            protected void onPostExecute(String token) {
                Log.i(TAG, "Access token retrieved:" + token);
                loginMvpPresenter.setAccessToken(token);
                new GetLoginData().execute();
            }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public class GetLoginData extends AsyncTask<Void, Void, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {

                OkHttpClient client = MyOkHttpClient.getNewHttpClient(loginMvpPresenter.getAccessToken());//httpClient.build();


                Request request = new Request.Builder()
                        .url("https://api.fusionbangladesh.com/login/index")
                        .build();

                try (Response response = client.newCall(request).execute()) {
                    return response.body().string();
                }

            } catch (Exception e) {
                e.printStackTrace();
                hideProgressBar();
                googleSignOut();
            }

            return getResources().getString(R.string.api_not_success);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgressBar();

            if(!TextUtils.isEmpty(s)){
                try {
                    JSONObject jo = new JSONObject(s);
                    if(jo.getString("message").equalsIgnoreCase("Success")){

                        String email = jo.getString("email");
                        String api_token = jo.getString("access_token");
                        String userName = jo.getString("username");
                        String role = jo.getString("role");
                        String appVersion = jo.getString("app_version");

                        Log.i("aaaEmail--:",email);
                        Log.i("aaaApi_token--:",api_token);
                        Log.i("aaaUsername--:",userName);
                        Log.i("aaaRole--:",role);
                        Log.i("aaaAppVersion--:",appVersion);

                        loginMvpPresenter.setLoginMode();
                        loginMvpPresenter.setEmailAddress(email);
                        loginMvpPresenter.setApiToken(api_token);
                        SharedPrefsHelper prefsHelper = new SharedPrefsHelper(LoginActivity.this);
                        prefsHelper.setRole(role);
                        prefsHelper.setUserName(userName);
                        prefsHelper.setAppVersion(appVersion);

                        loginMvpPresenter.openServiceActivity();

                    }
                    else{
                        showSnackBar("This is not registered mail address.");
                        googleSignOut();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showSnackBar(s);
                    googleSignOut();
                }
            }
        }
    }


    void showSnackBar(String message){

        View parentLayout = findViewById(R.id.sign_in_button);
        Snackbar snackbar = Snackbar.make(parentLayout,message,2000);
        View snackbarView = snackbar.getView();
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView mTextView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        mTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        snackbar.show();

    }


    void googleSignOut(){
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }
}
