package com.pkgroupmanagement.login;

import com.pkgroupmanagement.base.MvpPresenter;
import com.pkgroupmanagement.base.MvpView;

/**
 * Created by macbookpro on 12/9/17.
 */

public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {

    void setGoogleSignInRequest();
    void setLoginMode();
    void setEmailAddress(String email);
    void setApiToken(String api_token);
    void getApiToken();
    String getEmailAddress();
    void setAccessToken(String token);
    String getAccessToken();
    void openServiceActivity();

}
