package com.pkgroupmanagement.login;

import com.pkgroupmanagement.base.MvpView;

/**
 * Created by macbookpro on 12/9/17.
 */

public interface LoginMvpView extends MvpView {

     void openServiceActivity();
     void onGoogleSignInRequest();
}
