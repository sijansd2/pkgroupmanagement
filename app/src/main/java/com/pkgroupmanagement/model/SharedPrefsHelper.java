package com.pkgroupmanagement.model;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

import static android.content.Context.MODE_PRIVATE;



public class SharedPrefsHelper {

    public static final String MY_PREFS = "PK_GROUP_PREFS";

    public static final String email = "EMAIL";
    public static final String access_token = "accessToken";
    public static final String api_token = "apiToken";
    public static final String key_role = "com.key.pkgroup.api.role";
    public static final String key_app_version = "com.key.pkgroup.api.app_version";

    SharedPreferences mSharedPreferences;

    public SharedPrefsHelper(Context context) {
        mSharedPreferences = context.getSharedPreferences(MY_PREFS, MODE_PRIVATE);
    }

    public void clear() {
        mSharedPreferences.edit().clear().apply();
    }

    public void putEmail(String email) {
        mSharedPreferences.edit().putString(email, email).apply();
    }

    public String getEmail() {
        return mSharedPreferences.getString(email, null);
    }

    public void putAccessToken(String a) {
        mSharedPreferences.edit().putString(access_token, a).apply();
    }
    public String getAccessToken() {
        return mSharedPreferences.getString(access_token, null);
    }

    public void setApiToken(String a) {
        mSharedPreferences.edit().putString(api_token, a).apply();
    }
    public String getApiToken() {
        return mSharedPreferences.getString(api_token, null);
    }


    public void setUserName(String a) {
        mSharedPreferences.edit().putString("key_username", a).apply();
    }
    public String getUserName() {
        return mSharedPreferences.getString("key_username", null);
    }


    public boolean getLoggedInMode() {
        return mSharedPreferences.getBoolean("IS_LOGGED_IN", false);
    }

    public void setLoggedInMode(boolean loggedIn) {
        mSharedPreferences.edit().putBoolean("IS_LOGGED_IN", loggedIn).apply();
    }

    public String getRole() {
        return mSharedPreferences.getString(key_role, "");
    }

    public void setRole(String role) {
        mSharedPreferences.edit().putString(key_role, role).apply();
    }

    public String getAppVersion() {
        return mSharedPreferences.getString(key_app_version, null);
    }

    public void setAppVersion(String appVersion) {
        mSharedPreferences.edit().putString(key_app_version, appVersion).apply();
    }


    public String getClaimlist() {
        return mSharedPreferences.getString("ClaimList", null);
    }

    public void setClaimlist(String set) {
        mSharedPreferences.edit().putString("ClaimList", set).apply();
    }

    public String getDistricts() {
        return mSharedPreferences.getString("key_app_districr", null);
    }

    public void setDistricts(String set) {
        mSharedPreferences.edit().putString("key_app_districr", set).apply();
    }





    //================================
    //physical_condition
    public String get_physical_condition() {
        return mSharedPreferences.getString("physical_condition", null);
    }
    public void set_physical_condition(String physical_condition) {
        mSharedPreferences.edit().putString("physical_condition", physical_condition).apply();
    }


    //physical_plate_condition
    public String get_physical_plate_condition() {
        return mSharedPreferences.getString("physical_plate_condition", null);
    }
    public void set_physical_plate_condition(String physical_plate_condition) {
        mSharedPreferences.edit().putString("physical_plate_condition", physical_plate_condition).apply();
    }


    //physical_sulfation
    public String get_physical_sulfation() {
        return mSharedPreferences.getString("physical_sulfation", null);
    }
    public void set_physical_sulfation(String physical_sulfation) {
        mSharedPreferences.edit().putString("physical_sulfation", physical_sulfation).apply();
    }


    //physical_battery_acid
    public String get_physical_battery_acid() {
        return mSharedPreferences.getString("physical_battery_acid", null);
    }
    public void set_physical_battery_acid(String physical_battery_acid) {
        mSharedPreferences.edit().putString("physical_battery_acid", physical_battery_acid).apply();
    }





    //================================
    //initial_ocv
    public String get_initial_ocv() {
        return mSharedPreferences.getString("initial_ocv", null);
    }
    public void set_initial_ocv(String initial_ocv) {
        mSharedPreferences.edit().putString("initial_ocv", initial_ocv).apply();
    }

    //initial_spgr1
    public String get_initial_spgr1() {
        return mSharedPreferences.getString("initial_spgr1", null);
    }
    public void set_initial_spgr1(String initial_spgr1) {
        mSharedPreferences.edit().putString("initial_spgr1", initial_spgr1).apply();
    }

    //initial_spgr2
    public String get_initial_spgr2() {
        return mSharedPreferences.getString("initial_spgr2", null);
    }
    public void set_initial_spgr2(String initial_spgr2) {
        mSharedPreferences.edit().putString("initial_spgr2", initial_spgr2).apply();
    }

    //initial_spgr3
    public String get_initial_spgr3() {
        return mSharedPreferences.getString("initial_spgr3", null);
    }
    public void set_initial_spgr3(String initial_spgr3) {
        mSharedPreferences.edit().putString("initial_spgr3", initial_spgr3).apply();
    }

    //initial_spgr4
    public String get_initial_spgr4() {
        return mSharedPreferences.getString("initial_spgr4", null);
    }
    public void set_initial_spgr4(String initial_spgr4) {
        mSharedPreferences.edit().putString("initial_spgr4", initial_spgr4).apply();
    }

    //initial_spgr5
    public String get_initial_spgr5() {
        return mSharedPreferences.getString("initial_spgr5", null);
    }
    public void set_initial_spgr5(String initial_spgr5) {
        mSharedPreferences.edit().putString("initial_spgr5", initial_spgr5).apply();
    }

    //initial_spgr6
    public String get_initial_spgr6() {
        return mSharedPreferences.getString("initial_spgr6", null);
    }
    public void set_initial_spgr6(String initial_spgr6) {
        mSharedPreferences.edit().putString("initial_spgr6", initial_spgr6).apply();
    }

    //initial_load_volt
    public String get_initial_load_volt() {
        return mSharedPreferences.getString("initial_load_volt", null);
    }
    public void set_initial_load_volt(String initial_load_volt) {
        mSharedPreferences.edit().putString("initial_load_volt", initial_load_volt).apply();
    }

    //initial_remarks
    public String get_initial_remarks() {
        return mSharedPreferences.getString("initial_remarks", null);
    }
    public void set_initial_remarks(String initial_remarks) {
        mSharedPreferences.edit().putString("initial_remarks", initial_remarks).apply();
    }



    //================================
    //initial_discharging_battery_ah
    public String get_initial_discharging_battery_ah() {
        return mSharedPreferences.getString("initial_discharging_battery_ah", null);
    }
    public void set_initial_discharging_battery_ah(String initial_discharging_battery_ah) {
        mSharedPreferences.edit().putString("initial_discharging_battery_ah", initial_discharging_battery_ah).apply();
    }

    //initial_discharging_ah_dc
    public String get_initial_discharging_ah_dc() {
        return mSharedPreferences.getString("initial_discharging_ah_dc", null);
    }
    public void set_initial_discharging_ah_dc(String initial_discharging_ah_dc) {
        mSharedPreferences.edit().putString("initial_discharging_ah_dc", initial_discharging_ah_dc).apply();
    }

    //initial_discharging_ac_watt
    public String get_initial_discharging_ac_watt() {
        return mSharedPreferences.getString("initial_discharging_ac_watt", null);
    }
    public void set_initial_discharging_ac_watt(String initial_discharging_ac_watt) {
        mSharedPreferences.edit().putString("initial_discharging_ac_watt", initial_discharging_ac_watt).apply();
    }

    //initial_discharging_start_time
    public String get_initial_discharging_start_time() {
        return mSharedPreferences.getString("initial_discharging_start_time", null);
    }
    public void set_initial_discharging_start_time(String initial_discharging_start_time) {
        mSharedPreferences.edit().putString("initial_discharging_start_time", initial_discharging_start_time).apply();
    }

    //initial_discharging_end_time
    public String get_initial_discharging_end_time() {
        return mSharedPreferences.getString("initial_discharging_end_time", null);
    }
    public void set_initial_discharging_end_time(String initial_discharging_end_time) {
        mSharedPreferences.edit().putString("initial_discharging_end_time", initial_discharging_end_time).apply();
    }

    //initial_discharging_total_time
    public String get_initial_discharging_total_time() {
        return mSharedPreferences.getString("initial_discharging_total_time", null);
    }
    public void set_initial_discharging_total_time(String initial_discharging_total_time) {
        mSharedPreferences.edit().putString("initial_discharging_total_time", initial_discharging_total_time).apply();
    }

    //initial_discharging_remarks
    public String get_initial_discharging_remarks() {
        return mSharedPreferences.getString("initial_discharging_remarks", null);
    }
    public void set_initial_discharging_remarks(String initial_discharging_remarks) {
        mSharedPreferences.edit().putString("initial_discharging_remarks", initial_discharging_remarks).apply();
    }




    //================================
    //initial_physical_milage_start_time
    public String get_initial_physical_milage_start_time() {
        return mSharedPreferences.getString("initial_physical_milage_start_time", null);
    }
    public void set_initial_physical_milage_start_time(String initial_physical_milage_start_time) {
        mSharedPreferences.edit().putString("initial_physical_milage_start_time", initial_physical_milage_start_time).apply();
    }

    //initial_physical_milage_end_time
    public String get_initial_physical_milage_end_time() {
        return mSharedPreferences.getString("initial_physical_milage_end_time", null);
    }
    public void set_initial_physical_milage_end_time(String initial_physical_milage_end_time) {
        mSharedPreferences.edit().putString("initial_physical_milage_end_time", initial_physical_milage_end_time).apply();
    }

    //initial_physical_milage_total_time
    public String get_initial_physical_milage_total_time() {
        return mSharedPreferences.getString("initial_physical_milage_total_time", null);
    }
    public void set_initial_physical_milage_total_time(String initial_physical_milage_total_time) {
        mSharedPreferences.edit().putString("initial_physical_milage_total_time", initial_physical_milage_total_time).apply();
    }

    //initial_physical_milage_start
    public String get_initial_physical_milage_start() {
        return mSharedPreferences.getString("initial_physical_milage_start", null);
    }
    public void set_initial_physical_milage_start(String initial_physical_milage_start) {
        mSharedPreferences.edit().putString("initial_physical_milage_start", initial_physical_milage_start).apply();
    }

    //initial_physical_milage_end
    public String get_initial_physical_milage_end() {
        return mSharedPreferences.getString("initial_physical_milage_end", null);
    }
    public void set_initial_physical_milage_end(String initial_physical_milage_end) {
        mSharedPreferences.edit().putString("initial_physical_milage_end", initial_physical_milage_end).apply();
    }

    //initial_physical_milage_total
    public String get_initial_physical_milage_total() {
        return mSharedPreferences.getString("initial_physical_milage_total", null);
    }
    public void set_initial_physical_milage_total(String initial_physical_milage_total) {
        mSharedPreferences.edit().putString("initial_physical_milage_total", initial_physical_milage_total).apply();
    }

    //initial_physical_milage_remarks
    public String get_initial_physical_milage_remarks() {
        return mSharedPreferences.getString("initial_physical_milage_remarks", null);
    }
    public void set_initial_physical_milage_remarks(String initial_physical_milage_remarks) {
        mSharedPreferences.edit().putString("initial_physical_milage_remarks", initial_physical_milage_remarks).apply();
    }




    //================================
    //working_status
    public int get_working_status() {
        return mSharedPreferences.getInt("working_status", 0);
    }
    public void set_working_status(int working_status) {
        mSharedPreferences.edit().putInt("working_status", working_status).apply();
    }

    //working_change_water
    public int get_working_change_water() {
        return mSharedPreferences.getInt("working_change_water", 0);
    }
    public void set_working_change_water(int working_change_water) {
        mSharedPreferences.edit().putInt("working_change_water", working_change_water).apply();
    }

    //working_battery_charged
    public int get_working_battery_charged() {
        return mSharedPreferences.getInt("working_battery_charged", 0);
    }
    public void set_working_battery_charged(int working_battery_charged) {
        mSharedPreferences.edit().putInt("working_battery_charged", working_battery_charged).apply();
    }

    //working_adjusted_cell_gravity
    public int get_working_adjusted_cell_gravity() {
        return mSharedPreferences.getInt("working_adjusted_cell_gravity", 0);
    }
    public void set_working_adjusted_cell_gravity(int working_adjusted_cell_gravity) {
        mSharedPreferences.edit().putInt("working_adjusted_cell_gravity", working_adjusted_cell_gravity).apply();
    }

    //working_adjusted_acid_level
    public int get_working_adjusted_acid_level() {
        return mSharedPreferences.getInt("working_adjusted_acid_level", 0);
    }
    public void set_working_adjusted_acid_level(int working_adjusted_acid_level) {
        mSharedPreferences.edit().putInt("working_adjusted_acid_level", working_adjusted_acid_level).apply();
    }





    //================================
    //final_ocv
    public String get_final_ocv() {
        return mSharedPreferences.getString("final_ocv", null);
    }
    public void set_final_ocv(String final_ocv) {
        mSharedPreferences.edit().putString("final_ocv", final_ocv).apply();
    }

    //final_spgr1
    public String get_final_spgr1() {
        return mSharedPreferences.getString("final_spgr1", null);
    }
    public void set_final_spgr1(String final_spgr1) {
        mSharedPreferences.edit().putString("final_spgr1", final_spgr1).apply();
    }

    //final_spgr2
    public String get_final_spgr2() {
        return mSharedPreferences.getString("final_spgr2", null);
    }
    public void set_final_spgr2(String final_spgr2) {
        mSharedPreferences.edit().putString("final_spgr2", final_spgr2).apply();
    }

    //final_spgr3
    public String get_final_spgr3() {
        return mSharedPreferences.getString("final_spgr3", null);
    }
    public void set_final_spgr3(String final_spgr3) {
        mSharedPreferences.edit().putString("final_spgr3", final_spgr3).apply();
    }

    //final_spgr4
    public String get_final_spgr4() {
        return mSharedPreferences.getString("final_spgr4", null);
    }
    public void set_final_spgr4(String final_spgr4) {
        mSharedPreferences.edit().putString("final_spgr4", final_spgr4).apply();
    }

    //final_spgr5
    public String get_final_spgr5() {
        return mSharedPreferences.getString("final_spgr5", null);
    }
    public void set_final_spgr5(String final_spgr5) {
        mSharedPreferences.edit().putString("final_spgr5", final_spgr5).apply();
    }

    //final_spgr6
    public String get_final_spgr6() {
        return mSharedPreferences.getString("final_spgr6", null);
    }
    public void set_final_spgr6(String final_spgr6) {
        mSharedPreferences.edit().putString("final_spgr6", final_spgr6).apply();
    }

    //final_load_volt
    public String get_final_load_volt() {
        return mSharedPreferences.getString("final_load_volt", null);
    }
    public void set_final_load_volt(String final_load_volt) {
        mSharedPreferences.edit().putString("final_load_volt", final_load_volt).apply();
    }

    //final_remarks
    public String get_final_remarks() {
        return mSharedPreferences.getString("final_remarks", null);
    }
    public void set_final_remarks(String final_remarks) {
        mSharedPreferences.edit().putString("final_remarks", final_remarks).apply();
    }




    //================================
    //final_discharging_battery_ah
    public String get_final_discharging_battery_ah() {
        return mSharedPreferences.getString("final_discharging_battery_ah", null);
    }
    public void set_final_discharging_battery_ah(String final_discharging_battery_ah) {
        mSharedPreferences.edit().putString("final_discharging_battery_ah", final_discharging_battery_ah).apply();
    }

    //final_discharging_ah_dc
    public String get_final_discharging_ah_dc() {
        return mSharedPreferences.getString("final_discharging_ah_dc", null);
    }
    public void set_final_discharging_ah_dc(String final_discharging_ah_dc) {
        mSharedPreferences.edit().putString("final_discharging_ah_dc", final_discharging_ah_dc).apply();
    }

    //final_discharging_ac_watt
    public String get_final_discharging_ac_watt() {
        return mSharedPreferences.getString("final_discharging_ac_watt", null);
    }
    public void set_final_discharging_ac_watt(String final_discharging_ac_watt) {
        mSharedPreferences.edit().putString("final_discharging_ac_watt", final_discharging_ac_watt).apply();
    }

    //final_discharging_start_time
    public String get_final_discharging_start_time() {
        return mSharedPreferences.getString("final_discharging_start_time", null);
    }
    public void set_final_discharging_start_time(String final_discharging_start_time) {
        mSharedPreferences.edit().putString("final_discharging_start_time", final_discharging_start_time).apply();
    }

    //final_discharging_end_time
    public String get_final_discharging_end_time() {
        return mSharedPreferences.getString("final_discharging_end_time", null);
    }
    public void set_final_discharging_end_time(String final_discharging_end_time) {
        mSharedPreferences.edit().putString("final_discharging_end_time", final_discharging_end_time).apply();
    }

    //final_discharging_total_time
    public String get_final_discharging_total_time() {
        return mSharedPreferences.getString("final_discharging_total_time", null);
    }
    public void set_final_discharging_total_time(String final_discharging_total_time) {
        mSharedPreferences.edit().putString("final_discharging_total_time", final_discharging_total_time).apply();
    }

    //final_discharging_remarks
    public String get_final_discharging_remarks() {
        return mSharedPreferences.getString("final_discharging_remarks", null);
    }
    public void set_final_discharging_remarks(String final_discharging_remarks) {
        mSharedPreferences.edit().putString("final_discharging_remarks", final_discharging_remarks).apply();
    }



    //================================
    //final_physical_milage_start_time
    public String get_final_physical_milage_start_time() {
        return mSharedPreferences.getString("final_physical_milage_start_time", null);
    }
    public void set_final_physical_milage_start_time(String final_physical_milage_start_time) {
        mSharedPreferences.edit().putString("final_physical_milage_start_time", final_physical_milage_start_time).apply();
    }

    //final_physical_milage_end_time
    public String get_final_physical_milage_end_time() {
        return mSharedPreferences.getString("final_physical_milage_end_time", null);
    }
    public void set_final_physical_milage_end_time(String final_physical_milage_end_time) {
        mSharedPreferences.edit().putString("final_physical_milage_end_time", final_physical_milage_end_time).apply();
    }

    //final_physical_milage_total_time
    public String get_final_physical_milage_total_time() {
        return mSharedPreferences.getString("final_physical_milage_total_time", null);
    }
    public void set_final_physical_milage_total_time(String final_physical_milage_total_time) {
        mSharedPreferences.edit().putString("final_physical_milage_total_time", final_physical_milage_total_time).apply();
    }

    //final_physical_milage_start
    public String get_final_physical_milage_start() {
        return mSharedPreferences.getString("final_physical_milage_start", null);
    }
    public void set_final_physical_milage_start(String final_physical_milage_start) {
        mSharedPreferences.edit().putString("final_physical_milage_start", final_physical_milage_start).apply();
    }

    //final_physical_milage_end
    public String get_final_physical_milage_end() {
        return mSharedPreferences.getString("final_physical_milage_end", null);
    }
    public void set_final_physical_milage_end(String final_physical_milage_end) {
        mSharedPreferences.edit().putString("final_physical_milage_end", final_physical_milage_end).apply();
    }

    //final_physical_milage_total
    public String get_final_physical_milage_total() {
        return mSharedPreferences.getString("final_physical_milage_total", null);
    }
    public void set_final_physical_milage_total(String final_physical_milage_total) {
        mSharedPreferences.edit().putString("final_physical_milage_total", final_physical_milage_total).apply();
    }

    //final_physical_milage_remarks
    public String get_final_physical_milage_remarks() {
        return mSharedPreferences.getString("final_physical_milage_remarks", null);
    }
    public void set_final_physical_milage_remarks(String final_physical_milage_remarks) {
        mSharedPreferences.edit().putString("final_physical_milage_remarks", final_physical_milage_remarks).apply();
    }





    //================================
    //decision
    public String get_decision() {
        return mSharedPreferences.getString("decision", null);
    }
    public void set_decision(String decision) {
        mSharedPreferences.edit().putString("decision", decision).apply();
    }


    //================================
    //test_step
    public String get_test_step() {
        return mSharedPreferences.getString("test_step", "0");
    }
    public void set_test_step(String test_step) {
        mSharedPreferences.edit().putString("test_step", test_step).apply();
    }


    //================================
    //decision
    public String get_updated_at() {
        return mSharedPreferences.getString("updated_at", null);
    }
    public void set_updated_at(String updated_at) {
        mSharedPreferences.edit().putString("updated_at", updated_at).apply();
    }


    //================================
    //updated_by
    public String get_updated_by() {
        return mSharedPreferences.getString("updated_by", null);
    }
    public void set_updated_by(String updated_by) {
        mSharedPreferences.edit().putString("updated_by", updated_by).apply();
    }




    //================================
    //replaced_pid
    public String get_replaced_pid() {
        return mSharedPreferences.getString("replaced_pid", null);
    }
    public void set_replaced_pid(String replaced_pid) {
        mSharedPreferences.edit().putString("replaced_pid", replaced_pid).apply();
    }

    //================================
    //working_status
    public int get_finished() {
        return mSharedPreferences.getInt("finished", 0);
    }
    public void set_finished(int finished) {
        mSharedPreferences.edit().putInt("finished", finished).apply();
    }


    //================================
    //working_status
    public boolean get_is_new_test() {
        return mSharedPreferences.getBoolean("is_new_test", false);
    }
    public void set_is_new_test(boolean is_new_test) {
        mSharedPreferences.edit().putBoolean("is_new_test", is_new_test).apply();
    }

    //================================
    //decision_comments
    public String get_decision_comments() {
        return mSharedPreferences.getString("decision_comments", null);
    }
    public void set_decision_comments(String decision_comments) {
        mSharedPreferences.edit().putString("decision_comments", decision_comments).apply();
    }

}
