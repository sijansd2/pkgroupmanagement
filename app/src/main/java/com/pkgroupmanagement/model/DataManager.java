package com.pkgroupmanagement.model;



public class DataManager {

    SharedPrefsHelper mSharedPrefsHelper;

    public DataManager(SharedPrefsHelper sharedPrefsHelper) {
        mSharedPrefsHelper = sharedPrefsHelper;
    }

    public void clear() {
        mSharedPrefsHelper.clear();
    }

    public void saveEmailId(String email) {
        mSharedPrefsHelper.putEmail(email);
    }

    public String getEmailId() {
        return mSharedPrefsHelper.getEmail();
    }

    public void saveAccessToken(String a) {
        mSharedPrefsHelper.putAccessToken(a);
    }

    public String getAccessToken() {
        return mSharedPrefsHelper.getAccessToken();
    }

    public void setApiToken(String a) {
        mSharedPrefsHelper.setApiToken(a);
    }

    public String getApiToken() {
        return mSharedPrefsHelper.getApiToken();
    }

    public void setLoggedInMode() {
        mSharedPrefsHelper.setLoggedInMode(true);
    }

    public Boolean getLoggedInMode() {
        return mSharedPrefsHelper.getLoggedInMode();
    }
}
