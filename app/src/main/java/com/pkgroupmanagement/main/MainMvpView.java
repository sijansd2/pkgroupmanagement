package com.pkgroupmanagement.main;


import com.pkgroupmanagement.base.MvpView;

/**
 * Created by macbookpro on 12/9/17.
 */

public interface MainMvpView extends MvpView {

    void onGoogleSignOutRequest();
}
