package com.pkgroupmanagement.main;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.pkgroupmanagement.model.DataManager;
import com.pkgroupmanagement.R;
import com.pkgroupmanagement.base.BaseActivity;
import com.pkgroupmanagement.login.LoginActivity;
import com.pkgroupmanagement.utils.network.Connectivity;
import com.pkgroupmanagement.utils.ImageSize;
import com.pkgroupmanagement.utils.gps.MyGps;
import com.pkgroupmanagement.utils.gps.MyGpsService;
import com.pkgroupmanagement.utils.StoredDealers;
import com.pkgroupmanagement.utils.UserPermission;
import com.pkgroupmanagement.utils.network.MyOkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends BaseActivity implements MainMvpView , Handler.Callback{

    @BindView(R.id.existDealerYes)CheckBox existDealerYes;
    @BindView(R.id.existDealerNo)CheckBox existDealerNo;
    @BindView(R.id.haveVisitedYes)CheckBox haveVisitedYes;
    @BindView(R.id.haveVisitedNo)CheckBox haveVisitedNo;
    @BindView(R.id.anyChangeYes)CheckBox anyChangeYes;
    @BindView(R.id.anyChangeNo)CheckBox anyChangeNo;
    @BindView(R.id.checkCash)CheckBox checkCash;
    @BindView(R.id.checkCredit)CheckBox checkCredit;
    @BindView(R.id.checkPartial)CheckBox checkPartial;

    @BindView(R.id.spinner1)Spinner spinner1;
    @BindView(R.id.spinner2)Spinner spinner2;
    @BindView(R.id.captureButton)Button captureButton;
    @BindView(R.id.locationPhoto)ImageView locationPhoto;

    @BindView(R.id.viewSpinner1)LinearLayout viewSpinner1;
    @BindView(R.id.viewSpinner2)LinearLayout viewSpinner2;
    @BindView(R.id.viewEdit)LinearLayout viewEdit;
    @BindView(R.id.have_u_visited)LinearLayout have_u_visited;
    @BindView(R.id.anyChange)LinearLayout anyChange;

    @BindView(R.id.name)EditText name;
    @BindView(R.id.phone)EditText phone;
    @BindView(R.id.address)EditText address;
    @BindView(R.id.district)EditText district;
    @BindView(R.id.thana)EditText thana;
    @BindView(R.id.selling_brands)EditText selling_brands;
    @BindView(R.id.selling_cap)EditText selling_cap;
    @BindView(R.id.percent_of_ev)EditText percent_of_ev;
    @BindView(R.id.comments)EditText comments;
    @BindView(R.id.next)Button next;


    MainPresenter mainPresenter;
    public static Handler handler;
    private static final int CAMERA_REQUEST = 1888;
    MyGps myGps;
    private ProgressDialog pDialog;
    String[] existingDealersName;
    String[] nonExistingDealersName;
    ArrayList<StoredDealers> listExistDealers;
    ArrayList<StoredDealers> listNonExistDealers;
    int existDealerPosition = 0;
    int nonExistDealerPosition = 0;
    Double[] location;
    String encodedImage;


    public void showProgressBar() {
        ProgressDialog();
        pDialog.show();
    }

    public void hideProgressBar() {
        if(pDialog != null) {
            pDialog.dismiss();
        }
    }
    public  void ProgressDialog()
    {
        if(pDialog == null){
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
        }

    }


    public static Intent getStartIntent(Context context){
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        handler = new Handler(this);

        DataManager dataManager = ((MyApplication)getApplication()).getDataManager();
        mainPresenter = new MainPresenter(dataManager);
        mainPresenter.onAttach(this);

        if(Connectivity.isConnected(this)){
            new GetExistDealer().execute();
            new GetNonExistDealer().execute();
        }
        else {
            showSnackBar(getResources().getString(R.string.check_internet));
        }

    }

    @OnClick({
            R.id.existDealerYes,
            R.id.existDealerNo
    })
    void onExistingDealerClicked(CheckBox lightButton) {
        switch(lightButton.getId()) {
            case R.id.existDealerYes:
                setExistingView();
                break;

            case R.id.existDealerNo:
                setNonExistingView();
                break;

                default:
                    break;
        }
    }


    void setExistingView(){

        existDealerYes.setChecked(true);
        existDealerNo.setChecked(false);

        viewSpinner1.setVisibility(View.VISIBLE);
        viewSpinner2.setVisibility(View.GONE);
        spinner1.setSelection(0);
        spinner2.setSelection(0);
        setExistingSpinner();

        viewEdit.setVisibility(View.GONE);
        cleanViewEdit();

        have_u_visited.setVisibility(View.GONE);
        haveVisitedYes.setChecked(true);
        haveVisitedNo.setChecked(false);

        anyChange.setVisibility(View.GONE);
        anyChangeYes.setChecked(false);
        anyChangeNo.setChecked(true);
    }

    void setNonExistingView(){

        existDealerYes.setChecked(false);
        existDealerNo.setChecked(true);

        viewSpinner1.setVisibility(View.GONE);
        viewSpinner2.setVisibility(View.GONE);
        spinner1.setSelection(0);
        spinner2.setSelection(0);
        setNonExistingSpinner();

        viewEdit.setVisibility(View.GONE);
        cleanViewEdit();

        have_u_visited.setVisibility(View.VISIBLE);
        haveVisitedYes.setChecked(false);
        haveVisitedNo.setChecked(false);

        anyChange.setVisibility(View.GONE);
        anyChangeYes.setChecked(false);
        anyChangeNo.setChecked(true);
    }

    void cleanViewEdit(){
        setNonExistDealerEditView(0);
    }


    @OnClick({
            R.id.haveVisitedYes,
            R.id.haveVisitedNo
    })
    void onHaveVisitedClicked(CheckBox visited) {
        switch(visited.getId()) {
            case R.id.haveVisitedYes:
                haveVisitedYes.setChecked(true);
                haveVisitedNo.setChecked(false);
                viewEdit.setVisibility(View.GONE);
                viewSpinner2.setVisibility(View.VISIBLE);
                break;

            case R.id.haveVisitedNo:
                haveVisitedYes.setChecked(false);
                haveVisitedNo.setChecked(true);
                viewEdit.setVisibility(View.VISIBLE);
                name.requestFocus();
                setNonExistDealerEditView(0);
                setEditViewEnable(true);
                viewSpinner2.setVisibility(View.GONE);
                anyChange.setVisibility(View.GONE);
                anyChangeYes.setChecked(false);
                anyChangeNo.setChecked(false);
                spinner2.setSelection(0);
                break;

            default:
                break;
        }
    }

    @OnClick({
            R.id.checkCash,
            R.id.checkCredit,
            R.id.checkPartial
    })
    void onPaymentClicked(CheckBox visited) {
        switch(visited.getId()) {
            case R.id.checkCash:
                checkCash.setChecked(true);
                checkPartial.setChecked(false);
                checkCredit.setChecked(false);
                break;

            case R.id.checkCredit:
                checkCash.setChecked(false);
                checkPartial.setChecked(false);
                checkCredit.setChecked(true);
                break;

            case R.id.checkPartial:
                checkCash.setChecked(false);
                checkPartial.setChecked(true);
                checkCredit.setChecked(false);
                break;

            default:
                break;
        }
    }

    @OnClick({
            R.id.anyChangeYes,
            R.id.anyChangeNo
    })
    void onAnyChangeClicked(CheckBox visited) {
        switch(visited.getId()) {
            case R.id.anyChangeYes:
                anyChangeYes.setChecked(true);
                anyChangeNo.setChecked(false);
                setEditViewEnable(true);
                name.requestFocus();
                break;

            case R.id.anyChangeNo:
                anyChangeYes.setChecked(false);
                anyChangeNo.setChecked(true);
                setEditViewEnable(false);
                break;

            default:
                break;
        }
    }


    @OnClick(R.id.captureButton)
    void takePhoto(){

        if(existDealerYes.isChecked()){
            if(spinner1.getSelectedItemPosition() == 0){
                showSnackBar("Please choose a dealer");
                return;
            }
        }
        else if(existDealerNo.isChecked()){
            if(!haveVisitedYes.isChecked() && !haveVisitedNo.isChecked()){
                showSnackBar("Please choose if you visited any shop or not.");
                return;
            }
            else if(haveVisitedYes.isChecked() && spinner2.getSelectedItemPosition() == 0){
                showSnackBar("Please choose a dealer");
                return;
            }
        }


        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (android.os.Build.VERSION.SDK_INT > 22) {
            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION};

            if (!UserPermission.hasPermissions(this, PERMISSIONS)) {
                UserPermission.getLocationPermission(this);
                return;
            }
        }


            // Check if enabled and if not send user to the GPS settings
            if (!enabled) {
                showSettingsAlert();
            } else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }


    }


    @OnItemSelected(R.id.spinner1)
    public void spinnerItemSelected(Spinner spinner, int position) {

        if(position == 0){
                have_u_visited.setVisibility(View.GONE);
                anyChange.setVisibility(View.GONE);
                viewEdit.setVisibility(View.GONE);
                setNonExistDealerEditView(0);

        }else {
            setEditViewEnable(false);
            existDealerPosition = position;
            setNonExistDealerEditView(existDealerPosition);
            viewEdit.setVisibility(View.VISIBLE);
            have_u_visited.setVisibility(View.GONE);
            //anyChange.setVisibility(View.VISIBLE);
            haveVisitedYes.setChecked(true);
            haveVisitedNo.setChecked(false);
            anyChangeNo.setChecked(true);
            anyChangeYes.setChecked(false);
            locationPhoto.setVisibility(View.GONE);
            }
    }


    @OnItemSelected(R.id.spinner2)
    public void spinner2ItemSelect(Spinner spinner, int position){

        if(position == 0){
            anyChange.setVisibility(View.GONE);
            anyChangeYes.setChecked(false);
            anyChangeNo.setChecked(true);
        }
        else{
            nonExistDealerPosition = position;
            setNonExistDealerEditView(nonExistDealerPosition);
            anyChange.setVisibility(View.VISIBLE);
            viewEdit.setVisibility(View.VISIBLE);
            setEditViewEnable(false);
            anyChangeYes.setChecked(false);
            anyChangeNo.setChecked(true);
        }


    }

    @OnClick(R.id.next)
    void nextClicked(){


        if(existDealerYes.isChecked()){
            if(spinner1.getSelectedItemPosition() == 0){
                showSnackBar("Please choose a dealer");
                return;
            }
        }
        else if(existDealerNo.isChecked()){
            if(!haveVisitedYes.isChecked() && !haveVisitedNo.isChecked()){
                showSnackBar("Please choose if you visited any shop or not.");
                return;
            }
            else if(haveVisitedYes.isChecked() && spinner2.getSelectedItemPosition() == 0){
                showSnackBar("Please choose a dealer");
                return;
            }
        }
        gotoDetailPage();

//        if(haveVisitedYes.isChecked()){
//
//            int posi = 0;
//            if(existDealerYes.isChecked()){
//                posi = existDealerPosition;
//            }else {
//                posi = nonExistDealerPosition;
//            }
//            if(posi == 0){
//                showSnackBar(getResources().getString(R.string.select_dealer));
//                return;
//            }
//
//            if(anyChangeNo.isChecked()){
//                setNonExistDealerEditView(posi);
//            }
//            gotoDetailPage();
//        }
//        else {
//            gotoDetailPage();
//        }

    }


    private void setExistingSpinner() {
        if(existingDealersName == null)return;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, existingDealersName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
    }

    private void setNonExistingSpinner() {
        if(nonExistingDealersName == null)return;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, nonExistingDealersName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);
    }

    @OnClick(R.id.district)
    void districtClick(){
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle("Districts");
        ListView listView = (ListView) dialog.findViewById(R.id.List);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_spinner_dropdown_item, getDistrictNames());
        listView.setAdapter(adapter);
        dialog.show();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                district.setText((CharSequence) adapterView.getItemAtPosition(i));
                dialog.dismiss();
            }
        });

        //return false;
    }

    @Override
    public void onGoogleSignOutRequest() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getResources().getString(R.string.google_id_token))
                .requestEmail()
                .build();

        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        startActivity(LoginActivity.getStartIntent(MainActivity.this));
                        finish();
                    }
                });
    }


    void showSnackBar(String message){

        View parentLayout = findViewById(R.id.next);
        Snackbar snackbar = Snackbar.make(parentLayout,message,2000);
        View snackbarView = snackbar.getView();
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView mTextView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        mTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        snackbar.show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean handleMessage(Message message) {

        if(message.what == 1){
            location = (Double[]) message.obj;
            hideProgressBar();
            Log.i("Lat--",String.valueOf(location[0]));
            Log.i("Long--",String.valueOf(location[1]));
            stopService(new Intent(this, MyGpsService.class));
            //showDetail.setText(String.valueOf(message.obj));
        }

        if(message.what == 100){
            finish();
        }

        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            photo = ImageSize.getResizedBitmap(photo,150,150);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            locationPhoto.setVisibility(View.VISIBLE);
            locationPhoto.setImageBitmap(photo);
            Log.i("photoSize", String.valueOf(photo.getByteCount()));
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            //Log.i("photoBase64: ", encodedImage);
            getCurrentLocation();
        }
    }

    void getCurrentLocation(){
        showProgressBar();
        Log.i("GpsStatus","GPS is enable");
        startService(new Intent(this, MyGpsService.class));
    }



    void setEditViewEnable(boolean bool){

        name.setEnabled(bool);
        phone.setEnabled(bool);
        address.setEnabled(bool);
        district.setEnabled(bool);
        thana.setEnabled(bool);
        selling_brands.setEnabled(bool);
        selling_cap.setEnabled(bool);
        percent_of_ev.setEnabled(bool);
        checkCash.setClickable(bool);
        checkCredit.setClickable(bool);
        checkPartial.setClickable(bool);

    }

    void setNonExistDealerEditView(int pos){

        if(pos == 0){
            name.setText("");
            phone.setText("");
            address.setText("");
            district.setText("");
            thana.setText("");
            selling_brands.setText("");
            selling_cap.setText("");
            percent_of_ev.setText("");
            comments.setText("");
            checkPartial.setChecked(false);
            checkCredit.setChecked(false);
            checkCash.setChecked(false);
            locationPhoto.setVisibility(View.GONE);
        }
        else {
            StoredDealers sd;
            if(existDealerYes.isChecked()){
                sd = listExistDealers.get(pos - 1);
            }
            else{
                sd = listNonExistDealers.get(pos - 1);
            }

            if(sd == null)return;


            name.setText(sd.getName());
            phone.setText(sd.getPhone());
            address.setText(sd.getAddress());
            district.setText(sd.getDistrict());
            thana.setText(sd.getThana());
            selling_brands.setText(sd.getSelling_brands());
            selling_cap.setText(sd.getSelling_cap());
            percent_of_ev.setText(sd.getPercent_of_ev());

            if (sd.getPayment_system().equals("cash")) {
                checkCash.setChecked(true);
                checkCredit.setChecked(false);
                checkPartial.setChecked(false);
            } else if (sd.getPayment_system().equals("credit")) {
                checkCash.setChecked(false);
                checkCredit.setChecked(true);
                checkPartial.setChecked(false);
            } else {
                checkCash.setChecked(false);
                checkCredit.setChecked(false);
                checkPartial.setChecked(true);
            }
        }
    }



    public class GetExistDealer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            try {

                OkHttpClient client = MyOkHttpClient.getNewHttpClient(MyApplication.prefsHelper.getApiToken());//httpClient.build();


                Request request = new Request.Builder()
                        .url("https://api.fusionbangladesh.com/v2/my-dealers/?existing=true")
                        .build();

                try (Response response = client.newCall(request).execute()) {
                    return response.body().string();
                }

            } catch (Exception e) {
                e.printStackTrace();
                hideProgressBar();
            }

            return getResources().getString(R.string.api_not_success);
        }

        @Override
        protected void onPostExecute(String token) {
            hideProgressBar();
            if(!TextUtils.isEmpty(token)){
                try {
                    JSONArray jsonArray = new JSONArray(token);
                    Log.i("jsonArraySize: ",jsonArray.length()+"");
                    listExistDealers = new ArrayList<StoredDealers>();
                    existingDealersName = new String[jsonArray.length()+1];
                    existingDealersName[0] = "Choose an existing dealer";

                    if(jsonArray.length() > 0){
                        for(int i=0; i< jsonArray.length(); i++){
                            JSONObject jo = jsonArray.getJSONObject(i);
                            StoredDealers sd = new StoredDealers();
                            sd.setId(jo.getString("id"));
                            sd.setName(jo.getString("name"));
                            sd.setPhone(jo.getString("phone"));
                            sd.setAddress(jo.getString("address"));
                            sd.setDistrict(jo.getString("district"));
                            sd.setThana(jo.getString("thana"));
                            sd.setLatitude(jo.getString("latitude"));
                            sd.setLongitude(jo.getString("longitude"));
                            sd.setSelling_brands(jo.getString("selling_brands"));
                            sd.setSelling_cap(jo.getString("selling_cap"));
                            sd.setPercent_of_ev(jo.getString("percent_of_ev"));
                            sd.setPayment_system(jo.getString("payment_system"));
                            sd.setRegistered(jo.getString("registered"));
                            sd.setSro_id(jo.getString("sro_id"));
                            sd.setSro_visit_date(jo.getString("sro_visit_date"));
                            sd.setSro_progress(jo.getString("sro_progress"));
                            sd.setUpdated_at(jo.getString("updated_at"));
                            sd.setUpdated_by(jo.getString("updated_by"));

                            existingDealersName[i+1] = jo.getString("name");
                            listExistDealers.add(sd);

                        }

                        setExistingSpinner();

                    }else{
                        showSnackBar("There is no existing dealers");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    showSnackBar(token);
                }
            }
        }
    }

    public class GetNonExistDealer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            try {

                OkHttpClient client = MyOkHttpClient.getNewHttpClient(mainPresenter.getApiAccessToken());//httpClient.build();
                Request request = new Request.Builder()
                        .url("https://api.fusionbangladesh.com/v2/my-dealers/?existing=false")
                        .build();

                try (Response response = client.newCall(request).execute()) {
                    return response.body().string();
                }

            } catch (Exception e) {
                e.printStackTrace();
                hideProgressBar();
            }

            return getResources().getString(R.string.api_not_success);
        }

        @Override
        protected void onPostExecute(String token) {
            hideProgressBar();
            if(!TextUtils.isEmpty(token)){
                try {
                    JSONArray jsonArray = new JSONArray(token);
                    Log.i("jsonArraySize: ",jsonArray.length()+"");
                    listNonExistDealers = new ArrayList<StoredDealers>();
                    nonExistingDealersName = new String[jsonArray.length()+1];
                    nonExistingDealersName[0] = "Choose a non existing dealer";

                    if(jsonArray.length() > 0){
                        for(int i=0; i< jsonArray.length(); i++){
                            JSONObject jo = jsonArray.getJSONObject(i);
                            StoredDealers sd = new StoredDealers();
                            sd.setId(jo.getString("id"));
                            sd.setName(jo.getString("name"));
                            sd.setPhone(jo.getString("phone"));
                            sd.setAddress(jo.getString("address"));
                            sd.setDistrict(jo.getString("district"));
                            sd.setThana(jo.getString("thana"));
                            sd.setLatitude(jo.getString("latitude"));
                            sd.setLongitude(jo.getString("longitude"));
                            sd.setSelling_brands(jo.getString("selling_brands"));
                            sd.setSelling_cap(jo.getString("selling_cap"));
                            sd.setPercent_of_ev(jo.getString("percent_of_ev"));
                            sd.setPayment_system(jo.getString("payment_system"));
                            sd.setRegistered(jo.getString("registered"));
                            sd.setSro_id(jo.getString("sro_id"));
                            sd.setSro_visit_date(jo.getString("sro_visit_date"));
                            sd.setSro_progress(jo.getString("sro_progress"));
                            sd.setUpdated_at(jo.getString("updated_at"));
                            sd.setUpdated_by(jo.getString("updated_by"));

                            nonExistingDealersName[i+1] = jo.getString("name");
                            listNonExistDealers.add(sd);
                        }
                        setNonExistingSpinner();

                    }else{
                        showSnackBar("There is no existing dealers");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void showSettingsAlert() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        //Setting Dialog Title
        alertDialog.setTitle(R.string.GPSAlertDialogTitle);

        //Setting Dialog Message
        alertDialog.setMessage(R.string.GPSAlertDialogMessage);

        //On Pressing Setting button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        //On pressing cancel button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }



    void gotoDetailPage(){
        if(checkValidation().length()==0){

            StoredDealers sd = new StoredDealers();
            sd.setName(name.getText().toString());
            sd.setPhone(phone.getText().toString());
            sd.setAddress(address.getText().toString());
            sd.setDistrict(district.getText().toString());
            sd.setThana(thana.getText().toString());
            sd.setSelling_brands(selling_brands.getText().toString());
            sd.setSelling_cap(selling_cap.getText().toString());
            sd.setPercent_of_ev(percent_of_ev.getText().toString());
            sd.setComments(comments.getText().toString());
            sd.setLatitude(String.valueOf(location[0]));
            sd.setLongitude(String.valueOf(location[1]));
            sd.setTime(getTimeDate());

            if(!TextUtils.isEmpty(encodedImage)) {
                sd.setPhoto(encodedImage);
            }


            if(checkCash.isChecked()){
                sd.setPayment_system("cash");
            }
            else if(checkCredit.isChecked()){
                sd.setPayment_system("credit");
            }
            else if(checkPartial.isChecked()){
                sd.setPayment_system("partial");
            }

            if(existDealerYes.isChecked()){
                sd.setIsExistingDealer("yes");
            }else{
                sd.setIsExistingDealer("no");
            }

            if(haveVisitedYes.isChecked()){
                sd.setVisitedPreviously("yes");
                if(existDealerYes.isChecked()){
                    sd.setId(listExistDealers.get(existDealerPosition-1).getId());
                }
                else{
                    sd.setId(listNonExistDealers.get(nonExistDealerPosition-1).getId());
                }

            }else {
                if(existDealerYes.isChecked()){
                    sd.setId(listExistDealers.get(existDealerPosition-1).getId());
                }
                else{
                    sd.setId("");
                }
                sd.setVisitedPreviously("no");

            }

            if(anyChangeYes.isChecked()){
                sd.setInformationChange("yes");
            }else{
                sd.setInformationChange("no");
            }


            Intent intent = new Intent(this, SubmitPage.class);
            intent.putExtra("detail", sd);
            startActivity(intent);


        }else{
            showSnackBar(checkValidation());
        }

    }

    String checkValidation(){

        String mgs = "";

        if(TextUtils.isEmpty(name.getText().toString()))mgs += "Empty Shop name, ";
        if(TextUtils.isEmpty(phone.getText().toString()))mgs += "Empty phone, ";
        if(TextUtils.isEmpty(address.getText().toString()))mgs += "Empty address, ";
        if(TextUtils.isEmpty(district.getText().toString()))mgs += "Empty district, ";
        if(TextUtils.isEmpty(thana.getText().toString()))mgs += "Empty thana, ";
        if(TextUtils.isEmpty(selling_brands.getText().toString()))mgs += "Empty selling brands, ";
        if(TextUtils.isEmpty(selling_cap.getText().toString()))mgs += "Empty selling capacity, ";
        if(TextUtils.isEmpty(percent_of_ev.getText().toString()))mgs += "Empty percent of ev, ";
        if(!checkCash.isChecked() && !checkCredit.isChecked() && !checkPartial.isChecked())mgs += "No payment selected, ";
        if(locationPhoto.getVisibility() == View.GONE)mgs += "Please take photo";
        else return "";

        return mgs;
    }

    public String getTimeDate() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        String date_time = dateFormat.format(cal.getTime());
        return date_time;
    }


    public String[] getDistrictNames() {
        String json = null;
        String[] districtNames = null;
        try {
            InputStream is = getAssets().open("district.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        try {
            JSONArray jsonArray = new JSONArray(json);
            districtNames = new String[jsonArray.length()];
            for(int i=0; i<jsonArray.length(); i++){

                JSONObject jo = jsonArray.getJSONObject(i);
                districtNames[i] = jo.getString("name");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return districtNames;
    }
}
