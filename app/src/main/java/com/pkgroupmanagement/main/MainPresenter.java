package com.pkgroupmanagement.main;


import com.pkgroupmanagement.model.DataManager;
import com.pkgroupmanagement.base.BasePresenter;

/**
 * Created by macbookpro on 12/9/17.
 */

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements MainMvpPresenter<V> {

    public MainPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void setGoogleSignOutRequest() {
        getMvpView().onGoogleSignOutRequest();
        getDataManager().clear();
    }

    @Override
    public String getApiAccessToken() {
        return getDataManager().getApiToken();
    }
}
