package com.pkgroupmanagement.main;

import com.pkgroupmanagement.base.MvpPresenter;

/**
 * Created by macbookpro on 12/9/17.
 */

public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {

    void setGoogleSignOutRequest();
    String getApiAccessToken();

}
