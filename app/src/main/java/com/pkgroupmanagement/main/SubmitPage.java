package com.pkgroupmanagement.main;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pkgroupmanagement.R;
import com.pkgroupmanagement.base.BaseActivity;
import com.pkgroupmanagement.model.DataManager;
import com.pkgroupmanagement.utils.StoredDealers;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by macbookpro on 12/16/17.
 */

public class SubmitPage extends BaseActivity {

    @BindView(R.id.detailImage)ImageView image;
    @BindView(R.id.tvDetails)TextView tvDetails;
    @BindView(R.id.submit)Button submit;

    StoredDealers sd;
    private ProgressDialog pDialog;
    DataManager dataManager;
    String errorLog = "";
    public static final MediaType MEDIA_TYPE =
            MediaType.parse("application/json");

    public void showProgressBar() {
        ProgressDialog();
        pDialog.show();
    }

    public void hideProgressBar() {
        if(pDialog != null) {
            pDialog.dismiss();
        }
    }
    public  void ProgressDialog()
    {
        if(pDialog == null){
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
        }

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit);
        ButterKnife.bind(this);

        dataManager = ((MyApplication)getApplication()).getDataManager();

        sd = (StoredDealers) getIntent().getSerializableExtra("detail");

        if(sd == null)return;

//                    String data =   "id:-- "+sd.getId()+" \n"+
//                                    "previously visit:-- "+sd.getVisitedPreviously()+" \n"+
//                                    "information change:-- "+sd.getInformationChange()+" \n"+
//                                    "is exist:-- "+sd.getIsExistingDealer()+" \n"+



                String data =       "Shop Name:-- "+sd.getName()+" \n"+
                                    "Phone:-- "+sd.getPhone()+" \n"+
                                    "Address:-- "+sd.getAddress()+" \n"+
                                    "District:-- "+sd.getDistrict()+" \n"+
                                    "Thana:-- "+sd.getThana()+" \n"+
                                    "Latitude:-- "+sd.getLatitude()+" \n"+
                                    "Longitude:-- "+sd.getLongitude()+" \n"+
                                    "Selling Brands:-- "+sd.getSelling_brands()+" \n"+
                                    "Selling capacity:-- "+sd.getSelling_cap()+" \n"+
                                    "Percent of ev:-- "+sd.getPercent_of_ev()+" \n"+
                                    "Payment System:-- "+sd.getPayment_system()+" \n"+
                                    "Time:-- "+sd.getTime()+" \n"+
                                    "is Existing Dealer:-- "+sd.getIsExistingDealer()+" \n"+
                                    "Comments:-- "+sd.getComments()+" \n";

        byte[] decodedBytes = Base64.decode(sd.getPhoto(), 0);
        Bitmap bmp = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);

        image.setImageBitmap(bmp);
        tvDetails.setText(data);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.log:
                if(errorLog.equals("")){
                    //Toast.makeText(this, "No log",Toast.LENGTH_LONG).show();
                    showErrorAlert("No log");
                }else{
                    showErrorAlert(errorLog);
                    //Toast.makeText(this, errorLog,Toast.LENGTH_LONG).show();
                }

                break;

            default:
                break;
        }
        return(super.onOptionsItemSelected(item));
    }


    @OnClick(R.id.submit)
    void submitPressed(){
        new PostDealer().execute();
    }



    public class PostDealer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            try {

                final OkHttpClient client = new OkHttpClient();
                JSONObject postdata = new JSONObject();
                try {
                        postdata.put("shop_name", sd.getName());
                        postdata.put("is_existing_dealer", sd.getIsExistingDealer());
                        postdata.put("phone", sd.getPhone());
                        postdata.put("address", sd.getAddress());
                        postdata.put("district", sd.getDistrict());
                        postdata.put("selling_brands", sd.getSelling_brands());
                        postdata.put("selling_cap", sd.getSelling_cap());
                        postdata.put("percent_of_ev", sd.getPercent_of_ev());
                        postdata.put("payment_system", sd.getPayment_system());
                        postdata.put("visited_previously", sd.getVisitedPreviously());
                        postdata.put("thana", sd.getThana());
                        postdata.put("latitude", sd.getLatitude());
                        postdata.put("longitude", sd.getLongitude());
                        postdata.put("time_of_visit", sd.getTime());
                        postdata.put("dealer_id", sd.getId());
                        postdata.put("information_change", sd.getInformationChange());
                        postdata.put("picture", sd.getPhoto());
                        postdata.put("comments", sd.getComments());
                } catch(JSONException e){
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                RequestBody body = RequestBody.create(MEDIA_TYPE,
                        postdata.toString());

                final Request request = new Request.Builder()
                        .url("https://api.fusionbangladesh.com/v2/create")
                        .post(body)
                        .addHeader("Authorization", "Bearer "+dataManager.getApiToken())
                        .build();

                try (Response response = client.newCall(request).execute()) {
                    return response.body().string();
                }

            } catch (Exception e) {
                e.printStackTrace();
                hideProgressBar();
            }

            return getResources().getString(R.string.api_not_success);
        }

        @Override
        protected void onPostExecute(String response) {
            hideProgressBar();
            Log.i("PostResponse ", response);

            try {
                JSONObject jsonObject = new JSONObject(response);
                if(jsonObject.getString("success").equalsIgnoreCase("Submitted Successfully!")){
                    Message msg = Message.obtain(MainActivity.handler,100);
                    msg.sendToTarget();
                    Toast.makeText(SubmitPage.this,"Submit Successful!",Toast.LENGTH_LONG).show();
                    startActivity(MainActivity.getStartIntent(SubmitPage.this));
                    finish();
                }
                else {
                    Log.e("responseError",response);
                    errorLog = response;
                    showSnackBar("Something went wrong!");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                errorLog = response;
                showSnackBar("Something went wrong!");
            }


        }
    }


    void showSnackBar(String message){

        View parentLayout = findViewById(R.id.submit);
        Snackbar snackbar = Snackbar.make(parentLayout,message,2000);
        View snackbarView = snackbar.getView();
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView mTextView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        mTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        snackbar.show();

    }

    public void showErrorAlert(String s) {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        //Setting Dialog Title
        alertDialog.setTitle("Error Log");

        //Setting Dialog Message
        alertDialog.setMessage(s);

        //On pressing cancel button
        alertDialog.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

}
