/*
 *    Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.pkgroupmanagement.main;

import android.app.Application;

import com.pkgroupmanagement.model.DataManager;
import com.pkgroupmanagement.model.SharedPrefsHelper;


public class MyApplication extends Application {

    DataManager dataManager;
    public static SharedPrefsHelper prefsHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPrefsHelper sharedPrefsHelper = new SharedPrefsHelper(getApplicationContext());
        dataManager = new DataManager(sharedPrefsHelper);
        prefsHelper = sharedPrefsHelper;

    }

    public DataManager getDataManager() {
        return dataManager;
    }

}
