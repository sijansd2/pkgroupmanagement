package com.pkgroupmanagement.services.checkBattery;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pkgroupmanagement.R;
import com.pkgroupmanagement.login.LoginActivity;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.model.SharedPrefsHelper;
import com.pkgroupmanagement.services.createComplain.CreateComplainActivity;
import com.pkgroupmanagement.utils.UserPermission;
import com.pkgroupmanagement.utils.barCodeScanner.SimpleScannerActivity;
import com.pkgroupmanagement.utils.network.Connectivity;
import com.pkgroupmanagement.utils.network.MyOkHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class BatteryCheckActivity extends AppCompatActivity {

    @BindView(R.id.tvBatterCheck)TextView tvBatterCheck;
    @BindView(R.id.noItemImage)LinearLayout noItemImage;
    @BindView(R.id.etBarcode)EditText etBarcode;

    public static Intent getStartIntent(Context context){
        return new Intent(context, BatteryCheckActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battery_check);

        UserPermission.getCameraPermission(this);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);

        TextView title = toolbar.findViewById(R.id.title);
        title.setText("Battery Check");

        ImageView imvClose = toolbar.findViewById(R.id.back);
        imvClose.setVisibility(View.VISIBLE);
        imvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!TextUtils.isEmpty(CreateComplainActivity.pid))
            etBarcode.setText(CreateComplainActivity.pid);
    }

    @OnClick(R.id.barcodeScanner)
    void clickBarcodeScanner(){
        //Toast.makeText(this, "Not implemented yet", Toast.LENGTH_SHORT).show()
        Intent intent = new Intent(this,SimpleScannerActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnCreate)
    void clickBtnBatteryCheck(){
        //Toast.makeText(this, "Not implemented yet", Toast.LENGTH_SHORT).show();

        if(Connectivity.isConnected(this)){
            new CheckBattery().execute();
        }
        else{
            showSnackBar(getResources().getString(R.string.check_internet), findViewById(R.id.btnCreate));
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public class CheckBattery extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgressBar();
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {

                OkHttpClient client = MyOkHttpClient.getNewHttpClient(MyApplication.prefsHelper.getApiToken());//httpClient.build();


                Request request = new Request.Builder()
                        .url("http://api.fusionbangladesh.com/v3/product-model?pid="+etBarcode.getText().toString())
                        .build();

                try (Response response = client.newCall(request).execute()) {
                    return response.body().string();
                }

            } catch (Exception e) {
                e.printStackTrace();
                //hideProgressBar();
            }

            return getResources().getString(R.string.api_not_success);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //hideProgressBar();

            if(!TextUtils.isEmpty(s)){
                noItemImage.setVisibility(View.GONE);
                s=s.replaceAll(",", "\n");
                s=s.replaceAll("\\{", "");
                s=s.replaceAll("\\}", "");
                tvBatterCheck.setText(s);
            }

            else{
                noItemImage.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CreateComplainActivity.pid = null;
    }


    void showSnackBar(String message, View v){

        View parentLayout = v;
        Snackbar snackbar = Snackbar.make(parentLayout,message,2000);
        View snackbarView = snackbar.getView();
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView mTextView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        mTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        snackbar.show();

    }
}
