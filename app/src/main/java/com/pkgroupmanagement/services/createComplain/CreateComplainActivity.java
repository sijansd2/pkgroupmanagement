package com.pkgroupmanagement.services.createComplain;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.google.gson.JsonObject;
import com.pkgroupmanagement.R;
import com.pkgroupmanagement.api.APIService;
import com.pkgroupmanagement.api.ApiUtils;
import com.pkgroupmanagement.api.response.CreateComplainResponse;
import com.pkgroupmanagement.api.response.DealerListResponse;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.services.ServiceActivity;
import com.pkgroupmanagement.utils.UserPermission;
import com.pkgroupmanagement.utils.barCodeScanner.SimpleScannerActivity;
import com.pkgroupmanagement.utils.network.Connectivity;
import com.pkgroupmanagement.utils.network.MyOkHttpClient;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CreateComplainActivity extends AppCompatActivity implements CalendarDatePickerDialogFragment.OnDateSetListener{

    @BindView(R.id.datePicker)ImageView datePicker;
    @BindView(R.id.tvDate)TextView tvDate;
    @BindView(R.id.etBarcode)EditText etBarcode;
    @BindView(R.id.etServiceLocation)EditText etServiceLocation;
    //@BindView(R.id.etCustomerClaim)EditText etCustomerClaim;
    @BindView(R.id.claimSpinner)SearchableSpinner claimSpinner;
    @BindView(R.id.etDealerName)EditText etDealerName;

    String[] dealerNameArray = null;
    List<DealerListResponse> dealerList;
    int dealer_id;
    public static String pid;
    String service_location;
    String customer_claim;
    String date_production;
    ArrayAdapter<String> adapter;

    public static Intent getStartIntent(Context context){
        return new Intent(context, CreateComplainActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_complain);

        UserPermission.getCameraPermission(this);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);

        TextView title = toolbar.findViewById(R.id.title);
        title.setText("Create New Complain");

        ImageView imvClose = toolbar.findViewById(R.id.back);
        imvClose.setVisibility(View.VISIBLE);
        imvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if(MyApplication.prefsHelper.getClaimlist() != null){

            String[] list = MyApplication.prefsHelper.getClaimlist().split(",");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(CreateComplainActivity.this, android.R.layout.simple_spinner_item, list);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            claimSpinner.setAdapter(adapter);
        }
        else {
            new LoadClaim().execute();
        }


        claimSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tv = (TextView)view;
                if(tv == null) return;
                customer_claim = tv.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!TextUtils.isEmpty(pid))
        etBarcode.setText(pid);

        if(SearchDealerActivity.dealerItem != null && !TextUtils.isEmpty(SearchDealerActivity.dealerItem.name)){
            etDealerName.setText(SearchDealerActivity.dealerItem.name);
            dealer_id = SearchDealerActivity.dealerItem.id;
        }
    }

    @OnClick(R.id.datePicker)
    void clickDatePicker(){

        Calendar calendar = Calendar.getInstance();
        CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(CreateComplainActivity.this)
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setPreselectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .setDoneText("Ok")
                .setCancelText("Cancel")
                .setThemeCustom(R.style.MyCustomBetterPickersDialogs);
        cdp.show(getSupportFragmentManager(), "dateTag");
    }


    @OnClick(R.id.etDealerName)
    void etDealerNameClicked(){
        startActivity(SearchDealerActivity.getStartIntent(CreateComplainActivity.this));
    }


    @OnClick(R.id.barcodeScanner)
    void clickBarcodeScanner(){
        //Toast.makeText(this, "Not implemented yet", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this,SimpleScannerActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnCreate)
    void clickBtnCreate(){
        //Toast.makeText(this, "Not implemented yet", Toast.LENGTH_SHORT).show();

        if(Connectivity.isConnected(this)){
            createComplain();
        }
        else{
            showSnackBar(getResources().getString(R.string.check_internet), findViewById(R.id.btnCreate));
        }
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

        String selectedYear = year+"";
        String selectedMonth = monthOfYear+1+"";
        String selectedDay = dayOfMonth+"";
        tvDate.setText(selectedYear+"-"+selectedMonth+"-"+selectedDay);
    }


    private void createComplain(){

        pid = etBarcode.getText().toString();
        service_location = etServiceLocation.getText().toString();
        date_production = tvDate.getText().toString();

        if( TextUtils.isEmpty(dealer_id+"")
                ||  TextUtils.isEmpty(pid)
                || TextUtils.isEmpty(service_location)
                ||  TextUtils.isEmpty(customer_claim)
                ||  TextUtils.isEmpty(date_production)){

            showSnackBar("Please fill up all fields", etBarcode);

            return;
        }

        APIService apiService =  new ApiUtils().getAPIService();
        apiService.createComplain(dealer_id+"",pid,service_location,customer_claim,date_production)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<CreateComplainResponse>() {
                    @Override
                    public void onNext(CreateComplainResponse createComplainResponse) {
                        if(!TextUtils.isEmpty(createComplainResponse.errors)) {
                            Log.e("aaaComplainResponse", createComplainResponse.errors);
                            showSnackBar(createComplainResponse.errors, etBarcode);
                        }
                        else if(!TextUtils.isEmpty(createComplainResponse.success)){
                            Toast.makeText(CreateComplainActivity.this, createComplainResponse.success, Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    void showSnackBar(String message, View v){

        View parentLayout = v;
        Snackbar snackbar = Snackbar.make(parentLayout,message,2000);
        View snackbarView = snackbar.getView();
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView mTextView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        mTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        snackbar.show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pid = null;
        SearchDealerActivity.dealerItem = null;
    }




    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public class LoadClaim extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showProgressBar();
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {

                OkHttpClient client = MyOkHttpClient.getNewHttpClient(MyApplication.prefsHelper.getApiToken());//httpClient.build();


                Request request = new Request.Builder()
                        .url("https://api.fusionbangladesh.com/service/claim-list/")
                        .build();

                try (Response response = client.newCall(request).execute()) {
                    return response.body().string();
                }

            } catch (Exception e) {
                e.printStackTrace();
                //hideProgressBar();
            }

            return getResources().getString(R.string.api_not_success);
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //hideProgressBar()

            JSONObject claim = null;
            try {
                claim = new JSONObject(s);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray keys = claim.names();

            List<String> list = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < keys.length(); i++){
                list.add(keys.optString(i));
                sb.append(list.get(i)).append(",");
            }

            MyApplication.prefsHelper.setClaimlist(sb.toString());

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(CreateComplainActivity.this, android.R.layout.simple_spinner_item, list);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            claimSpinner.setAdapter(adapter);
        }
    }
}
