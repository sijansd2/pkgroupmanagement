package com.pkgroupmanagement.services.createComplain;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pkgroupmanagement.R;
import com.pkgroupmanagement.adapter.SearchDealerAdapter;
import com.pkgroupmanagement.api.APIService;
import com.pkgroupmanagement.api.ApiUtils;
import com.pkgroupmanagement.api.response.DealerListResponse;
import com.pkgroupmanagement.services.checkBattery.BatteryCheckActivity;
import com.pkgroupmanagement.utils.network.Connectivity;

import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

public class SearchDealerActivity extends AppCompatActivity {

    @BindView(R.id.rvSearchedDealers)RecyclerView rvSearchedDealers;
    @BindView(R.id.btnClose)Button btnClose;
    @BindView(R.id.etSearch)EditText etSearch;

    List<DealerListResponse> dealerList;
    SearchDealerAdapter adapter;
    public static DealerListResponse dealerItem;


    private ProgressDialog pDialog;
    public void showProgressBar() {
        ProgressDialog();
        pDialog.show();
    }

    public void hideProgressBar() {
        if(pDialog != null) {
            pDialog.dismiss();
        }
    }
    public  void ProgressDialog()
    {
        if(pDialog == null){
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
        }

    }

    public static Intent getStartIntent(Context context){
        return new Intent(context, SearchDealerActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_dealer);

        ButterKnife.bind(this);

        dealerList = new ArrayList<>();
        adapter = new SearchDealerAdapter(this,dealerList);
        rvSearchedDealers.setAdapter(adapter);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvSearchedDealers.setLayoutManager(mLayoutManager);
        rvSearchedDealers.setItemAnimator(new DefaultItemAnimator());

        loadDealerForCreateComplain();
    }

    @OnClick(R.id.btnClose)
    public void btnClosedPressed(){
        finish();
    }

    @OnTextChanged(R.id.etSearch)
    public void searchTextChange(CharSequence s, int start, int before, int count){

        if(Connectivity.isConnected(this)){
            if(s.length() >= 3){
                loadSearchDealers(s.toString());
            }
            else if(s.length() == 0){
                loadDealerForCreateComplain();
            }
        }
        else{
            showSnackBar(getResources().getString(R.string.check_internet), findViewById(R.id.btnCreate));
            return;
        }

    }


    private void loadDealerForCreateComplain(){

        showProgressBar();
        APIService apiService = new ApiUtils().getAPIService();
        apiService.dealerListForComplainCreate()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<List<DealerListResponse>>() {
                    @Override
                    public void onNext(List<DealerListResponse> dealerList) {

                        if(dealerList == null)return;

                        adapter.clearData();
                        adapter.addData(dealerList);
                        adapter.notifyDataSetChanged();
                        hideProgressBar();
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        hideProgressBar();
                    }

                    @Override
                    public void onComplete() {
                        hideProgressBar();
                    }
                });
    }


    private void loadSearchDealers(String str){

        APIService apiService = new ApiUtils().getAPIService();
        apiService.searchDealer(str)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<List<DealerListResponse>>() {
                    @Override
                    public void onNext(List<DealerListResponse> dealerList) {

                        adapter.clearData();
                        adapter.addData(dealerList);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void showSnackBar(String message, View v){

        View parentLayout = v;
        Snackbar snackbar = Snackbar.make(parentLayout,message,2000);
        View snackbarView = snackbar.getView();
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView mTextView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        mTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        snackbar.show();

    }
}
