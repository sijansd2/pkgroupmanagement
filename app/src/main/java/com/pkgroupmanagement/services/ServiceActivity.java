package com.pkgroupmanagement.services;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.pkgroupmanagement.R;
import com.pkgroupmanagement.login.LoginActivity;
import com.pkgroupmanagement.main.MainActivity;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.model.SharedPrefsHelper;
import com.pkgroupmanagement.services.checkBattery.BatteryCheckActivity;
import com.pkgroupmanagement.services.complainList.ComplainListActivity;
import com.pkgroupmanagement.services.createComplain.CreateComplainActivity;
import com.pkgroupmanagement.utils.network.Connectivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceActivity extends AppCompatActivity {

    public static String KEY_CLICK_TYPE = "com.key.pk.app.clickType";
    public static String KEY_CLICK_SRO = "com.key.pk.app.clickSro";
    public static String KEY_CLICK_SERVICE_EXE = "com.key.pk.app.clickServiceExe";

    @BindView(R.id.btnSales) LinearLayout btnSales;
    @BindView(R.id.username) TextView username;

    public static Intent getStartIntent(Context context){
        return new Intent(context, ServiceActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);


        TextView title = toolbar.findViewById(R.id.title);
        title.setText("Services");

        TextView logout = toolbar.findViewById(R.id.logout);
        logout.setVisibility(View.VISIBLE);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Connectivity.isConnected(ServiceActivity.this)){
                    logout();
                }
                else{
                    showSnackBar(getResources().getString(R.string.check_internet));
                }
            }
        });

        if(!TextUtils.isEmpty(MyApplication.prefsHelper.getUserName())){
            username.setText(MyApplication.prefsHelper.getUserName());
        }

        if(getIntent() != null){

            SharedPrefsHelper prefsHelper = new SharedPrefsHelper(this);

            if(getIntent().getStringExtra(KEY_CLICK_TYPE).equals(KEY_CLICK_SERVICE_EXE)){
                //prefsHelper.setIsSro(false);
                btnSales.setVisibility(View.GONE);
            }
            else{
                btnSales.setVisibility(View.VISIBLE);
                //prefsHelper.setIsSro(true);
            }
        }
    }


    public void salesClick(View view){
        //Toast.makeText(this, "salesClick", Toast.LENGTH_SHORT).show();
        Intent intent = MainActivity.getStartIntent(this);
        startActivity(intent);
    }

    public void createComplainClick(View view){
        //Toast.makeText(this, "createComplainClick", Toast.LENGTH_SHORT).show();
        Intent intent = CreateComplainActivity.getStartIntent(this);
        startActivity(intent);
    }

    public void complainListClick(View view){
        //Toast.makeText(this, "complainListClick", Toast.LENGTH_SHORT).show();
        Intent intent = ComplainListActivity.getStartIntent(this);
        startActivity(intent);
    }

    public void batteryCheckClick(View view){
        //Toast.makeText(this, "batteryCheckClick", Toast.LENGTH_SHORT).show();
        Intent intent = BatteryCheckActivity.getStartIntent(this);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }


    void logout(){

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getResources().getString(R.string.google_id_token))
                .requestEmail()
                .build();

        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        startActivity(LoginActivity.getStartIntent(ServiceActivity.this));
                        finish();
                    }
                });

        MyApplication.prefsHelper.clear();
    }


    void showSnackBar(String message){

        View parentLayout = findViewById(R.id.viewToolBar);
        Snackbar snackbar = Snackbar.make(parentLayout,message,2000);
        View snackbarView = snackbar.getView();
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView mTextView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        mTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        snackbar.show();

    }
}
