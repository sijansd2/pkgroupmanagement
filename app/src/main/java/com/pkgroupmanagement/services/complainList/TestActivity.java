package com.pkgroupmanagement.services.complainList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.pkgroupmanagement.R;
import com.pkgroupmanagement.api.APIService;
import com.pkgroupmanagement.api.ApiUtils;
import com.pkgroupmanagement.api.response.TestStatusGetResponse;
import com.pkgroupmanagement.api.response.TestStatusSaveResponse;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.utils.CustomViewPager;
import com.pkgroupmanagement.utils.UserPermission;

import java.io.PrintWriter;
import java.io.StringWriter;

import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;
import me.relex.circleindicator.CircleIndicator;

public class TestActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener{

    CustomViewPager vpPager;


    public String id;
    public String service_id;
    public String pid;
    public String replaced_pid;

    public String physical_condition = "";
    public String physical_plate_condition ="";
    public String physical_sulfation = "";
    public String physical_battery_acid = "";

    public double initial_ocv;
    public double initial_spgr1;
    public double initial_spgr2;
    public double initial_spgr3;
    public double initial_spgr4;
    public double initial_spgr5;
    public double initial_spgr6;
    public double initial_load_volt;
    public String initial_remarks;

    public double initial_discharging_battery_ah;
    public double initial_discharging_ah_dc;
    public double initial_discharging_ac_watt;
    public String initial_discharging_start_time;
    public String initial_discharging_end_time;
    public String initial_discharging_total_time;
    public String initial_discharging_remarks;

    public String initial_physical_milage_start_time;
    public String initial_physical_milage_end_time;
    public String initial_physical_milage_total_time;
    public double initial_physical_milage_start;
    public double initial_physical_milage_end;
    public double initial_physical_milage_total;
    public String initial_physical_milage_remarks;

    public int working_status;
    public int working_change_water;
    public int working_battery_charged;
    public int working_adjusted_cell_gravity;
    public int working_adjusted_acid_level;

    public double final_ocv;
    public double final_spgr1;
    public double final_spgr2;
    public double final_spgr3;
    public double final_spgr4;
    public double final_spgr5;
    public double final_spgr6;
    public double final_load_volt;
    public String final_remarks;

    public double final_discharging_battery_ah;
    public double final_discharging_ah_dc;
    public double final_discharging_ac_watt;
    public String final_discharging_start_time;
    public String final_discharging_end_time;
    public String final_discharging_total_time;
    public String final_discharging_remarks;

    public String final_physical_milage_start_time;
    public String final_physical_milage_end_time;
    public String final_physical_milage_total_time;
    public double final_physical_milage_start;
    public double final_physical_milage_end;
    public double final_physical_milage_total;
    public String final_physical_milage_remarks;

    public String decision = "pending";
    public String decision_comments = "";
    public int test_step =0;

    public static Unbinder unbinder;



    public static Intent getStartIntent(Context context){
        return new Intent(context, TestActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        UserPermission.getCameraPermission(this);

        if(getIntent() != null){
            if(!TextUtils.isEmpty(getIntent().getStringExtra("id"))){
                id = getIntent().getStringExtra("id");
                service_id = id;

                //if(getIntent()!= null && !getIntent().getStringExtra("isNew").equals("1")){
                    getTestStatus();
//                }
//                else{
//                    setViewPager();
//                }

            }

            if(!TextUtils.isEmpty(getIntent().getStringExtra("pid"))){
                pid = getIntent().getStringExtra("pid");
            }
        }
    }

    public void setPageSwipeEnable(boolean isSwipe){
        if(vpPager != null){
            vpPager.setPagingEnabled(isSwipe);
        }
    }

    private void setViewPager(){

        vpPager = (CustomViewPager) findViewById(R.id.viewPager);
        MyPagerAdapter adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        vpPager.setOnPageChangeListener(this);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(vpPager);

        if(Integer.parseInt(MyApplication.prefsHelper.get_test_step()) >0){
            vpPager.setCurrentItem(Integer.parseInt(MyApplication.prefsHelper.get_test_step()));
            vpPager.setPagingEnabled(true);
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        test_step = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 4;

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new PhysicalTestFragment();
                case 1:
                    return new InitialTestFragment();
                case 2:
                    return new WorkingStatusFragment();
                case 3:
                    return new DecisionFragment();
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }


    public void clickSaveBtn(){
        saveTestStatus();
    }

    public void saveTestStatus(){

        if(physical_condition.equals("--")){
            physical_condition = null;
        }

        if(physical_battery_acid.equals("--")){
            physical_battery_acid = null;
        }

        if(physical_plate_condition.equals("--")){
            physical_plate_condition = null;
        }

        if(physical_sulfation.equals("--")){
            physical_sulfation = null;
        }

        APIService apiService = new ApiUtils().getAPIService();
        apiService.makeTest(
                Integer.valueOf(id),
                Integer.valueOf(service_id),
                pid,
                replaced_pid,
                initial_ocv,
                initial_spgr1,
                initial_spgr2,
                initial_spgr3,
                initial_spgr4,
                initial_spgr5,
                initial_spgr6,
                initial_load_volt,
                initial_remarks,

                physical_condition,
                physical_plate_condition,
                physical_sulfation,
                physical_battery_acid,
                initial_discharging_battery_ah,
                initial_discharging_ah_dc,
                initial_discharging_ac_watt,
                initial_discharging_start_time,
                initial_discharging_end_time,
                initial_discharging_total_time,
                initial_discharging_remarks,
                initial_physical_milage_start_time,
                initial_physical_milage_end_time,
                initial_physical_milage_total_time,
                initial_physical_milage_start,
                initial_physical_milage_end,
                initial_physical_milage_total,
                initial_physical_milage_remarks,

                working_status,
                working_change_water,
                working_battery_charged,
                working_adjusted_cell_gravity,
                working_adjusted_acid_level,

                final_ocv,
                final_spgr1,
                final_spgr2,
                final_spgr3,
                final_spgr4,
                final_spgr5,
                final_spgr6,
                final_load_volt,
                final_remarks,

                final_discharging_battery_ah,
                final_discharging_ah_dc,
                final_discharging_ac_watt,
                final_discharging_start_time,
                final_discharging_end_time,
                final_discharging_total_time,
                final_discharging_remarks,

                final_physical_milage_start_time,
                final_physical_milage_end_time,
                final_physical_milage_total_time,
                final_physical_milage_start,
                final_physical_milage_end,
                final_physical_milage_total,
                final_physical_milage_remarks,

                decision,
                decision_comments,
                test_step)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<TestStatusSaveResponse>() {
                    @Override
                    public void onNext(TestStatusSaveResponse testStatusSaveResponse) {
                        if(!TextUtils.isEmpty(testStatusSaveResponse.success)){

                            Toast.makeText(TestActivity.this, testStatusSaveResponse.success, Toast.LENGTH_SHORT).show();
                            if(vpPager.getCurrentItem() == 3){
                                finish();
                            }
                        }

                        else if(!TextUtils.isEmpty(testStatusSaveResponse.error)){
                            Toast.makeText(TestActivity.this, testStatusSaveResponse.error, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        StringWriter errors = new StringWriter();
                        t.printStackTrace(new PrintWriter(errors));
                        Toast.makeText(TestActivity.this, errors.toString(), Toast.LENGTH_LONG).show();
                        t.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    void getTestStatus(){

        APIService apiService = new ApiUtils().getAPIService();
        apiService.getTestStatus(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<TestStatusGetResponse>() {
                    @Override
                    public void onNext(TestStatusGetResponse testStatusGetResponse) {
                        //Log.e("aaaTestStatus", testStatusGetResponse.toString());

                        if(!TextUtils.isEmpty(testStatusGetResponse.error)){
                            //Toast.makeText(TestActivity.this, testStatusGetResponse.error, Toast.LENGTH_LONG).show();
                            MyApplication.prefsHelper.set_is_new_test(true);
                            MyApplication.prefsHelper.set_test_step("0");
                            setViewPager();
                        }
                        else {
                            MyApplication.prefsHelper.set_is_new_test(false);

                            // ********* Physical test part ***********
                            MyApplication.prefsHelper.set_physical_condition(testStatusGetResponse.physical_condition);
                            physical_condition = testStatusGetResponse.physical_condition;

                            MyApplication.prefsHelper.set_physical_plate_condition(testStatusGetResponse.physical_plate_condition);
                            physical_plate_condition = testStatusGetResponse.physical_plate_condition;

                            MyApplication.prefsHelper.set_physical_sulfation(testStatusGetResponse.physical_sulfation);
                            physical_sulfation = testStatusGetResponse.physical_sulfation;

                            MyApplication.prefsHelper.set_physical_battery_acid(testStatusGetResponse.physical_battery_acid);
                            physical_battery_acid = testStatusGetResponse.physical_battery_acid;

                            // ********* Initial test part ***********
                            MyApplication.prefsHelper.set_initial_ocv(testStatusGetResponse.initial_ocv);
                            initial_ocv = Double.parseDouble(testStatusGetResponse.initial_ocv);
                            MyApplication.prefsHelper.set_initial_spgr1(testStatusGetResponse.initial_spgr1);
                            initial_spgr1 = Double.parseDouble(testStatusGetResponse.initial_spgr1);
                            MyApplication.prefsHelper.set_initial_spgr2(testStatusGetResponse.initial_spgr2);
                            initial_spgr2 = Double.parseDouble(testStatusGetResponse.initial_spgr2);
                            MyApplication.prefsHelper.set_initial_spgr3(testStatusGetResponse.initial_spgr3);
                            initial_spgr3 = Double.parseDouble(testStatusGetResponse.initial_spgr3);
                            MyApplication.prefsHelper.set_initial_spgr4(testStatusGetResponse.initial_spgr4);
                            initial_spgr4 = Double.parseDouble(testStatusGetResponse.initial_spgr4);
                            MyApplication.prefsHelper.set_initial_spgr5(testStatusGetResponse.initial_spgr5);
                            initial_spgr5 = Double.parseDouble(testStatusGetResponse.initial_spgr5);
                            MyApplication.prefsHelper.set_initial_spgr6(testStatusGetResponse.initial_spgr6);
                            initial_spgr6 = Double.parseDouble(testStatusGetResponse.initial_spgr6);
                            MyApplication.prefsHelper.set_initial_load_volt(testStatusGetResponse.initial_load_volt);
                            initial_load_volt = Double.parseDouble(testStatusGetResponse.initial_load_volt);
                            MyApplication.prefsHelper.set_initial_remarks(testStatusGetResponse.initial_remarks);
                            initial_remarks = testStatusGetResponse.initial_spgr1;

                            MyApplication.prefsHelper.set_initial_discharging_battery_ah(testStatusGetResponse.initial_discharging_battery_ah);
                            initial_discharging_battery_ah = Double.parseDouble(testStatusGetResponse.initial_discharging_battery_ah);
                            MyApplication.prefsHelper.set_initial_discharging_ah_dc(testStatusGetResponse.initial_discharging_ah_dc);
                            initial_discharging_ah_dc = Double.parseDouble(testStatusGetResponse.initial_discharging_ah_dc);
                            MyApplication.prefsHelper.set_initial_discharging_ac_watt(testStatusGetResponse.initial_discharging_ac_watt);
                            initial_discharging_ac_watt = Double.parseDouble(testStatusGetResponse.initial_discharging_ac_watt);
                            MyApplication.prefsHelper.set_initial_discharging_start_time(testStatusGetResponse.initial_discharging_start_time);
                            initial_discharging_start_time = testStatusGetResponse.initial_discharging_start_time;
                            MyApplication.prefsHelper.set_initial_discharging_end_time(testStatusGetResponse.initial_discharging_end_time);
                            initial_discharging_end_time = testStatusGetResponse.initial_discharging_end_time;
                            MyApplication.prefsHelper.set_initial_discharging_total_time(testStatusGetResponse.initial_discharging_total_time);
                            initial_discharging_total_time = testStatusGetResponse.initial_discharging_total_time;
                            MyApplication.prefsHelper.set_initial_discharging_remarks(testStatusGetResponse.initial_discharging_remarks);
                            initial_discharging_remarks = testStatusGetResponse.initial_discharging_remarks;

                            MyApplication.prefsHelper.set_initial_physical_milage_start_time(testStatusGetResponse.initial_physical_milage_start_time);
                            initial_physical_milage_start_time = testStatusGetResponse.initial_physical_milage_start_time;
                            MyApplication.prefsHelper.set_initial_physical_milage_end_time(testStatusGetResponse.initial_physical_milage_end_time);
                            initial_physical_milage_end_time = testStatusGetResponse.initial_physical_milage_end_time;
                            MyApplication.prefsHelper.set_initial_physical_milage_total_time(testStatusGetResponse.initial_physical_milage_total_time);
                            initial_physical_milage_total_time = testStatusGetResponse.initial_physical_milage_total_time;
                            MyApplication.prefsHelper.set_initial_physical_milage_start(testStatusGetResponse.initial_physical_milage_start);
                            initial_physical_milage_start = Double.parseDouble(testStatusGetResponse.initial_physical_milage_start);
                            MyApplication.prefsHelper.set_initial_physical_milage_end(testStatusGetResponse.initial_physical_milage_end);
                            initial_physical_milage_end = Double.parseDouble(testStatusGetResponse.initial_physical_milage_end);
                            MyApplication.prefsHelper.set_initial_physical_milage_total(testStatusGetResponse.initial_physical_milage_total);
                            initial_physical_milage_total = Double.parseDouble(testStatusGetResponse.initial_physical_milage_total);
                            MyApplication.prefsHelper.set_initial_physical_milage_remarks(testStatusGetResponse.initial_physical_milage_remarks);
                            initial_physical_milage_remarks = testStatusGetResponse.initial_physical_milage_remarks;


                            // ********* final test part ***********
                            MyApplication.prefsHelper.set_final_ocv(testStatusGetResponse.final_ocv);
                            final_ocv = Double.parseDouble(testStatusGetResponse.final_ocv);
                            MyApplication.prefsHelper.set_final_spgr1(testStatusGetResponse.final_spgr1);
                            final_spgr1 = Double.parseDouble(testStatusGetResponse.final_spgr1);
                            MyApplication.prefsHelper.set_final_spgr2(testStatusGetResponse.final_spgr2);
                            final_spgr2 = Double.parseDouble(testStatusGetResponse.final_spgr2);
                            MyApplication.prefsHelper.set_final_spgr3(testStatusGetResponse.final_spgr3);
                            final_spgr3 = Double.parseDouble(testStatusGetResponse.final_spgr3);
                            MyApplication.prefsHelper.set_final_spgr4(testStatusGetResponse.final_spgr4);
                            final_spgr4 = Double.parseDouble(testStatusGetResponse.final_spgr4);
                            MyApplication.prefsHelper.set_final_spgr5(testStatusGetResponse.final_spgr5);
                            final_spgr5 = Double.parseDouble(testStatusGetResponse.final_spgr5);
                            MyApplication.prefsHelper.set_final_spgr6(testStatusGetResponse.final_spgr6);
                            final_spgr6 = Double.parseDouble(testStatusGetResponse.final_spgr6);
                            MyApplication.prefsHelper.set_final_load_volt(testStatusGetResponse.final_load_volt);
                            final_load_volt = Double.parseDouble(testStatusGetResponse.final_load_volt);
                            MyApplication.prefsHelper.set_final_remarks(testStatusGetResponse.final_remarks);
                            final_remarks = testStatusGetResponse.final_spgr1;

                            MyApplication.prefsHelper.set_final_discharging_battery_ah(testStatusGetResponse.final_discharging_battery_ah);
                            final_discharging_battery_ah = Double.parseDouble(testStatusGetResponse.final_discharging_battery_ah);
                            MyApplication.prefsHelper.set_final_discharging_ah_dc(testStatusGetResponse.final_discharging_ah_dc);
                            final_discharging_ah_dc = Double.parseDouble(testStatusGetResponse.final_discharging_ah_dc);
                            MyApplication.prefsHelper.set_final_discharging_ac_watt(testStatusGetResponse.final_discharging_ac_watt);
                            final_discharging_ac_watt = Double.parseDouble(testStatusGetResponse.final_discharging_ac_watt);
                            MyApplication.prefsHelper.set_final_discharging_start_time(testStatusGetResponse.final_discharging_start_time);
                            final_discharging_start_time = testStatusGetResponse.final_discharging_start_time;
                            MyApplication.prefsHelper.set_final_discharging_end_time(testStatusGetResponse.final_discharging_end_time);
                            final_discharging_end_time = testStatusGetResponse.final_discharging_end_time;
                            MyApplication.prefsHelper.set_final_discharging_total_time(testStatusGetResponse.final_discharging_total_time);
                            final_discharging_total_time = testStatusGetResponse.final_discharging_total_time;
                            MyApplication.prefsHelper.set_final_discharging_remarks(testStatusGetResponse.final_discharging_remarks);
                            final_discharging_remarks = testStatusGetResponse.final_discharging_remarks;

                            MyApplication.prefsHelper.set_final_physical_milage_start_time(testStatusGetResponse.final_physical_milage_start_time);
                            final_physical_milage_start_time = testStatusGetResponse.final_physical_milage_start_time;
                            MyApplication.prefsHelper.set_final_physical_milage_end_time(testStatusGetResponse.final_physical_milage_end_time);
                            final_physical_milage_end_time = testStatusGetResponse.final_physical_milage_end_time;
                            MyApplication.prefsHelper.set_final_physical_milage_total_time(testStatusGetResponse.final_physical_milage_total_time);
                            final_physical_milage_total_time = testStatusGetResponse.final_physical_milage_total_time;
                            MyApplication.prefsHelper.set_final_physical_milage_start(testStatusGetResponse.final_physical_milage_start);
                            final_physical_milage_start = Double.parseDouble(testStatusGetResponse.final_physical_milage_start);
                            MyApplication.prefsHelper.set_final_physical_milage_end(testStatusGetResponse.final_physical_milage_end);
                            final_physical_milage_end = Double.parseDouble(testStatusGetResponse.final_physical_milage_end);
                            MyApplication.prefsHelper.set_final_physical_milage_total(testStatusGetResponse.final_physical_milage_total);
                            final_physical_milage_total = Double.parseDouble(testStatusGetResponse.final_physical_milage_total);
                            MyApplication.prefsHelper.set_final_physical_milage_remarks(testStatusGetResponse.final_physical_milage_remarks);
                            final_physical_milage_remarks = testStatusGetResponse.final_physical_milage_remarks;


                            MyApplication.prefsHelper.set_working_status(testStatusGetResponse.working_status);
                            working_status = testStatusGetResponse.working_status;
                            MyApplication.prefsHelper.set_working_change_water(testStatusGetResponse.working_change_water);
                            working_change_water = testStatusGetResponse.working_change_water;
                            MyApplication.prefsHelper.set_working_battery_charged(testStatusGetResponse.working_battery_charged);
                            working_battery_charged = testStatusGetResponse.working_battery_charged;
                            MyApplication.prefsHelper.set_working_adjusted_cell_gravity(testStatusGetResponse.working_adjusted_cell_gravity);
                            working_adjusted_cell_gravity = testStatusGetResponse.working_adjusted_cell_gravity;
                            MyApplication.prefsHelper.set_working_adjusted_acid_level(testStatusGetResponse.working_adjusted_acid_level);
                            working_adjusted_acid_level = testStatusGetResponse.working_adjusted_acid_level;

                            MyApplication.prefsHelper.set_decision(testStatusGetResponse.decision);
                            decision = testStatusGetResponse.decision;

                            MyApplication.prefsHelper.set_decision_comments(testStatusGetResponse.decision_comments);
                            decision_comments = testStatusGetResponse.decision_comments;

                            MyApplication.prefsHelper.set_replaced_pid(testStatusGetResponse.replaced_pid);
                            replaced_pid = testStatusGetResponse.replaced_pid;

                            MyApplication.prefsHelper.set_test_step(testStatusGetResponse.test_step + "");
                            test_step = testStatusGetResponse.test_step;


                            setViewPager();
                        }
                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
