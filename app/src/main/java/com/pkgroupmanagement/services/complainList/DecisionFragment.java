package com.pkgroupmanagement.services.complainList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.pkgroupmanagement.R;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.services.createComplain.CreateComplainActivity;
import com.pkgroupmanagement.services.createComplain.SearchDealerActivity;
import com.pkgroupmanagement.utils.barCodeScanner.SimpleScannerActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class DecisionFragment extends Fragment {

    @BindView(R.id.productIdLay)LinearLayout productIdLay;
    @BindView(R.id.decisionSpinner)Spinner decisionSpinner;
    @BindView(R.id.etBarcode)EditText etBarcode;
    @BindView(R.id.barcodeScannerDecision)ImageView barcodeScannerDecision;
    @BindView(R.id.etComments)EditText etComments;

    TestActivity activity;

    public static Intent getStartIntent(Context context){
        return new Intent(context, DecisionFragment.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (TestActivity)getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(!TextUtils.isEmpty(CreateComplainActivity.pid))
        etBarcode.setText(CreateComplainActivity.pid);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.view_decision, container, false);
        ButterKnife.bind(this, view);

        decisionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(i == 3){
                    productIdLay.setVisibility(View.VISIBLE);
                }
                else{
                    productIdLay.setVisibility(View.GONE);
                }

                TextView tv = (TextView)view;
                if(tv == null){
                    return;
                }
                else if(tv.getText().toString().equals("Pending Replacement")){
                    activity.decision = "pending_rep";
                }
                else if(!tv.getText().toString().equals("--")){
                    activity.decision = tv.getText().toString().toLowerCase();
                }
                else {
                    activity.decision = "pending";
                }

            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if(!MyApplication.prefsHelper.get_is_new_test()){
            setAllSpinners();
        }

        return view;
    }

    @OnClick(R.id.barcodeScannerDecision)
    void barCodeIconClicked(){
        Intent intent = new Intent(getActivity(),SimpleScannerActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnSave)
    void saveClicked(){
        //Toast.makeText(getActivity(), "Not implemented yet", Toast.LENGTH_SHORT).show();
        activity.decision_comments = etComments.getText().toString();
        activity.saveTestStatus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CreateComplainActivity.pid = null;
    }


    private void setAllSpinners() {

        if (!TextUtils.isEmpty(MyApplication.prefsHelper.get_decision())) {
            //String[] bc = getActivity().getResources().getStringArray(R.array.decision_array);

            if(MyApplication.prefsHelper.get_decision().equalsIgnoreCase("pending")){
                decisionSpinner.setSelection(0);
            }
            else if(MyApplication.prefsHelper.get_decision().equalsIgnoreCase("repaired")){
                decisionSpinner.setSelection(1);
            }
            else if(MyApplication.prefsHelper.get_decision().equalsIgnoreCase("rejected")){
                decisionSpinner.setSelection(2);
            }
            else if(MyApplication.prefsHelper.get_decision().equalsIgnoreCase("replaced")){
                decisionSpinner.setSelection(3);
            }
            else if(MyApplication.prefsHelper.get_decision().equalsIgnoreCase("pending_rep")){
                decisionSpinner.setSelection(4);
            }
        }

        if(!TextUtils.isEmpty(MyApplication.prefsHelper.get_decision_comments())){
            etComments.setText(MyApplication.prefsHelper.get_decision_comments());
        }

        if (!TextUtils.isEmpty(MyApplication.prefsHelper.get_replaced_pid()) && productIdLay.getVisibility() == View.VISIBLE) {
            etBarcode.setText(MyApplication.prefsHelper.get_replaced_pid());
        }
    }
}
