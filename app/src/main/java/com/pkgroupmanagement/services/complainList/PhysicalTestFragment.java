package com.pkgroupmanagement.services.complainList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pkgroupmanagement.R;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.utils.OnSwipeTouchListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhysicalTestFragment extends Fragment {

    @BindView(R.id.parentView)LinearLayout parentView;
    @BindView(R.id.spinnerBatteryCondition)Spinner spinnerBatteryCondition;
    @BindView(R.id.spinnerPlateCondition)Spinner spinnerPlateCondition;
    @BindView(R.id.spinnerSulfation)Spinner spinnerSulfation;
    @BindView(R.id.spinnerBatteryAcid)Spinner spinnerBatteryAcid;
    @BindView(R.id.btnSave)Button btnSave;

    TestActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.view_physical_test, container, false);
        ButterKnife.bind(this, view);
        setSpinnerValidation();

        if(!MyApplication.prefsHelper.get_is_new_test()){
            setAllSpinners();
        }

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (TestActivity) getActivity();
    }

    @OnClick(R.id.btnSave)
    void saveClicked(){
        activity.clickSaveBtn();
    }

    private void setSpinnerValidation(){

        spinnerBatteryCondition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                enablePageScrolling();
                TextView tv = (TextView)view;
                if(tv == null){
                    return;
                }
                activity.physical_condition = tv.getText().toString().toLowerCase();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerPlateCondition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                enablePageScrolling();
                TextView tv = (TextView)view;
                if(tv == null) return;
                activity.physical_plate_condition = tv.getText().toString().toLowerCase();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerSulfation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                enablePageScrolling();
                TextView tv = (TextView)view;
                if(tv == null) return;
                activity.physical_sulfation = tv.getText().toString().toLowerCase();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerBatteryAcid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                enablePageScrolling();
                TextView tv = (TextView)view;
                if(tv == null) return;
                activity.physical_battery_acid = tv.getText().toString().toLowerCase();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void enablePageScrolling(){

        TestActivity testActivity = (TestActivity)getActivity();
        if(spinnerBatteryCondition.getSelectedItemPosition() != 0
                && spinnerPlateCondition.getSelectedItemPosition() != 0
                && spinnerSulfation.getSelectedItemPosition() != 0
                && spinnerBatteryAcid.getSelectedItemPosition() != 0){

            testActivity.setPageSwipeEnable(true);
            btnSave.setEnabled(true);
        }
        else {
            testActivity.setPageSwipeEnable(false);
            btnSave.setEnabled(false);

            parentView.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
                public void onSwipeTop() {
                    //Toast.makeText(getActivity(), "top", Toast.LENGTH_SHORT).show();
                }
                public void onSwipeRight() {
                    //Toast.makeText(getActivity(), "right", Toast.LENGTH_SHORT).show();
                }
                public void onSwipeLeft() {
                    Toast.makeText(getActivity(), "All fields are required.", Toast.LENGTH_SHORT).show();
                }
                public void onSwipeBottom() {
                    //Toast.makeText(getActivity(), "bottom", Toast.LENGTH_SHORT).show();
                }

            });
        }
    }

    private void setAllSpinners(){

        if(!TextUtils.isEmpty(MyApplication.prefsHelper.get_physical_condition())){
            String[] bc = getActivity().getResources().getStringArray(R.array.battery_condition_array);
            for(int i=0; i<bc.length; i++){
                if(bc[i].equalsIgnoreCase(MyApplication.prefsHelper.get_physical_condition()))
                    spinnerBatteryCondition.setSelection(i);
            }

        }

        if(!TextUtils.isEmpty(MyApplication.prefsHelper.get_physical_plate_condition())){
            String[] bc = getActivity().getResources().getStringArray(R.array.plate_condition_array);
            for(int i=0; i<bc.length; i++){
                if(bc[i].equalsIgnoreCase(MyApplication.prefsHelper.get_physical_plate_condition()))
                    spinnerPlateCondition.setSelection(i);
            }

        }


        if(!TextUtils.isEmpty(MyApplication.prefsHelper.get_physical_sulfation())){
            String[] bc = getActivity().getResources().getStringArray(R.array.sulfation_array);
            for(int i=0; i<bc.length; i++){
                if(bc[i].equalsIgnoreCase(MyApplication.prefsHelper.get_physical_sulfation()))
                    spinnerSulfation.setSelection(i);
            }

        }

        if(!TextUtils.isEmpty(MyApplication.prefsHelper.get_physical_battery_acid())){
            String[] bc = getActivity().getResources().getStringArray(R.array.battery_acid_array);
            for(int i=0; i<bc.length; i++){
                if(bc[i].equalsIgnoreCase(MyApplication.prefsHelper.get_physical_battery_acid()))
                    spinnerBatteryAcid.setSelection(i);
            }

        }
    }


}
