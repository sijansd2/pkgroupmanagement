package com.pkgroupmanagement.services.complainList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.pkgroupmanagement.R;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.utils.OnSwipeTouchListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorkingStatusFragment extends Fragment
        implements CalendarDatePickerDialogFragment.OnDateSetListener, RadialTimePickerDialogFragment.OnTimeSetListener{

    @BindView(R.id.workingStatusParentView)LinearLayout workingStatusParentView;
    @BindView(R.id.wsCheckYes)CheckBox wsCheckYes;
    @BindView(R.id.wsCheckNo)CheckBox wsCheckNo;
    @BindView(R.id.waterChangeYes)CheckBox waterChangeYes;
    @BindView(R.id.waterChangeNo)CheckBox waterChangeNo;
    @BindView(R.id.cellGravityAdjustYes)CheckBox cellGravityAdjustYes;
    @BindView(R.id.cellGravityAdjustNo)CheckBox cellGravityAdjustNo;
    @BindView(R.id.acidLabelAdjustedYes)CheckBox acidLabelAdjustedYes;
    @BindView(R.id.acidLabelAdjustedNo)CheckBox acidLabelAdjustedNo;
    @BindView(R.id.batteryChangeYes)CheckBox batteryChangeYes;
    @BindView(R.id.batteryChangeNo)CheckBox batteryChangeNo;
    @BindView(R.id.initialFieldLay)LinearLayout initialFieldLay;
    @BindView(R.id.initialOptionalFieldText)LinearLayout initialOptionalFieldText;
    @BindView(R.id.childCheckOptions)LinearLayout childCheckOptions;
    @BindView(R.id.workingStatusInitialOptionalFieldLay)LinearLayout initialOptionalFieldLay;
    @BindView(R.id.initialTestTitle)TextView initialTestTitle;
    @BindView(R.id.testOptionalFieldTitle)TextView testOptionalFieldTitle;

    @BindView(R.id.ocv)EditText ocv;
    @BindView(R.id.loadVoltage)EditText loadVoltage;
    @BindView(R.id.spgr1)EditText spgr1;
    @BindView(R.id.spgr2)EditText spgr2;
    @BindView(R.id.spgr3)EditText spgr3;
    @BindView(R.id.spgr4)EditText spgr4;
    @BindView(R.id.spgr5)EditText spgr5;
    @BindView(R.id.spgr6)EditText spgr6;
    @BindView(R.id.remarks)EditText remarks;

    @BindView(R.id.batteryAhFinal)EditText batteryAh;
    @BindView(R.id.ahDcFinal)EditText ahDc;
    @BindView(R.id.acWattFinal)EditText acWatt;
    @BindView(R.id.dischargingStartTimeLayFinal)LinearLayout dischargingStartTimeLay;
    @BindView(R.id.dischargingEndTimeLayFinal)LinearLayout dischargingEndTimeLay;
    @BindView(R.id.dischargingStartTimeFinal)TextView dischargingStartTime;
    @BindView(R.id.dischargingEndTimeFinal)TextView dischargingEndTime;
    @BindView(R.id.dischargingTotalTimeFinal)TextView dischargingTotalTime;
    @BindView(R.id.dischargingRemarksFinal)EditText dischargingRemarks;

    @BindView(R.id.physicalStartTimeFinal)TextView physicalStartTime;
    @BindView(R.id.physicalEndTimeFinal)TextView physicalEndTime;
    @BindView(R.id.physicalTotalTimeFinal)TextView physicalTotalTime;
    @BindView(R.id.startMilageFinal)EditText startMilage;
    @BindView(R.id.endMilageFinal)EditText endMilage;
    @BindView(R.id.totalMilageFinal)EditText totalMilage;
    @BindView(R.id.physicalRemarksFinal)EditText physicalRemarks;

    TestActivity activity;
    String time;
    boolean isDischargeStartTimeClicked = false;
    boolean isPhysicalStartTimeClicked = false;
    boolean isDischargePart = false;
    String startTime, endTime;


    public static Intent getStartIntent(Context context){
        return new Intent(context, WorkingStatusFragment.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (TestActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.view_working_status, container, false);
        ButterKnife.bind(this, view);

        initialOptionalFieldText.setVisibility(View.GONE);
        initialTestTitle.setText("Final Test");
        testOptionalFieldTitle.setText("Final Discharging Report");


        if(!MyApplication.prefsHelper.get_is_new_test()){
            setAllFields();
        }
        return view;
    }


    @OnClick(R.id.dischargingStartTimeLayFinal)
    public void finalDischargeStartTimeClicked(){
        isDischargePart = true;
        isDischargeStartTimeClicked = true;
        setCalender();
    }

    @OnClick(R.id.dischargingEndTimeLayFinal)
    public void finalDischargeEndTimeClicked(){
        isDischargePart = true;
        isDischargeStartTimeClicked = false;
        setCalender();
    }

    @OnClick(R.id.physicalStartTimeLayFinal)
    public void finalPhysicalStartTimeLayClicked(){
        isDischargePart = false;
        isPhysicalStartTimeClicked = true;
        setCalender();
    }

    @OnClick(R.id.physicalEndTimeLayFinal)
    public void finalPhysicalEndTimeLayClicked(){
        isDischargePart = false;
        isPhysicalStartTimeClicked = false;
        setCalender();
    }


    @OnClick({
            R.id.wsCheckYes,
            R.id.wsCheckNo
    })
    void workingStatusChecked(CheckBox visited) {
        switch(visited.getId()) {
            case R.id.wsCheckYes:
                wsCheckYes.setChecked(true);
                wsCheckNo.setChecked(false);
                initialFieldLay.setVisibility(View.VISIBLE);
                childCheckOptions.setVisibility(View.VISIBLE);
                activity.working_status = 1;
                break;

            case R.id.wsCheckNo:
                wsCheckYes.setChecked(false);
                wsCheckNo.setChecked(true);
                initialFieldLay.setVisibility(View.GONE);
                childCheckOptions.setVisibility(View.GONE);
                activity.working_status = 0;
                break;

            default:
                break;
        }
    }


    @OnClick({
            R.id.waterChangeYes,
            R.id.waterChangeNo
    })
    void waterChangeChecked(CheckBox visited) {
        switch(visited.getId()) {
            case R.id.waterChangeYes:
                waterChangeYes.setChecked(true);
                waterChangeNo.setChecked(false);
                activity.working_change_water = 1;
                break;

            case R.id.waterChangeNo:
                waterChangeYes.setChecked(false);
                waterChangeNo.setChecked(true);
                activity.working_change_water = 0;
                break;

            default:
                break;
        }
    }

    @OnClick({
            R.id.batteryChangeYes,
            R.id.batteryChangeNo
    })
    void batteryChangeChecked(CheckBox visited) {
        switch(visited.getId()) {
            case R.id.batteryChangeYes:
                batteryChangeYes.setChecked(true);
                batteryChangeNo.setChecked(false);
                activity.working_battery_charged = 1;
                break;

            case R.id.batteryChangeNo:
                batteryChangeYes.setChecked(false);
                batteryChangeNo.setChecked(true);
                activity.working_battery_charged = 0;
                break;

            default:
                break;
        }
    }


    @OnClick({
            R.id.acidLabelAdjustedYes,
            R.id.acidLabelAdjustedNo
    })
    void acidLabelAdjustedChecked(CheckBox visited) {
        switch(visited.getId()) {
            case R.id.acidLabelAdjustedYes:
                acidLabelAdjustedYes.setChecked(true);
                acidLabelAdjustedNo.setChecked(false);
                activity.working_adjusted_acid_level = 1;
                break;

            case R.id.acidLabelAdjustedNo:
                acidLabelAdjustedYes.setChecked(false);
                acidLabelAdjustedNo.setChecked(true);
                activity.working_adjusted_acid_level = 0;
                break;

            default:
                break;
        }
    }



    @OnClick({
            R.id.cellGravityAdjustYes,
            R.id.cellGravityAdjustNo
    })
    void cellGravityAdjustChecked(CheckBox visited) {
        switch(visited.getId()) {
            case R.id.cellGravityAdjustYes:
                cellGravityAdjustYes.setChecked(true);
                cellGravityAdjustNo.setChecked(false);
                activity.working_adjusted_cell_gravity = 1;
                break;

            case R.id.cellGravityAdjustNo:
                cellGravityAdjustYes.setChecked(false);
                cellGravityAdjustNo.setChecked(true);
                activity.working_adjusted_cell_gravity = 0;
                break;

            default:
                break;
        }
    }

    @OnClick(R.id.testOptionalFieldText)
    void initialPageOptionalFieldClick(){
        if(initialOptionalFieldLay.getVisibility() == View.VISIBLE){
            initialOptionalFieldLay.setVisibility(View.GONE);
        }else {
            initialOptionalFieldLay.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btnSave)
    void saveClicked(){
        //Toast.makeText(getActivity(), "Not implemented yet", Toast.LENGTH_SHORT).show();

        if(!TextUtils.isEmpty(ocv.getText().toString()))
            activity.final_ocv = Double.parseDouble(ocv.getText().toString());

        if(!TextUtils.isEmpty(loadVoltage.getText().toString()))
            activity.final_load_volt = Double.parseDouble(loadVoltage.getText().toString());

        if(!TextUtils.isEmpty(spgr1.getText().toString()))
            activity.final_spgr1 = Double.parseDouble(spgr1.getText().toString());

        if(!TextUtils.isEmpty(spgr2.getText().toString()))
            activity.final_spgr2 = Double.parseDouble(spgr2.getText().toString());

        if(!TextUtils.isEmpty(spgr3.getText().toString()))
            activity.final_spgr3 = Double.parseDouble(spgr3.getText().toString());

        if(!TextUtils.isEmpty(spgr4.getText().toString()))
            activity.final_spgr4 = Double.parseDouble(spgr4.getText().toString());

        if(!TextUtils.isEmpty(spgr5.getText().toString()))
            activity.final_spgr5 = Double.parseDouble(spgr5.getText().toString());

        if(!TextUtils.isEmpty(spgr6.getText().toString()))
            activity.final_spgr6 = Double.parseDouble(spgr6.getText().toString());

        activity.final_remarks = remarks.getText().toString();

        if(!TextUtils.isEmpty(batteryAh.getText().toString()))
            activity.final_discharging_battery_ah = Double.parseDouble(batteryAh.getText().toString());

        if(!TextUtils.isEmpty(ahDc.getText().toString()))
            activity.final_discharging_ah_dc = Double.parseDouble(ahDc.getText().toString());

        if(!TextUtils.isEmpty(acWatt.getText().toString()))
            activity.final_discharging_ac_watt = Double.parseDouble(acWatt.getText().toString());

        activity.final_discharging_start_time = dischargingStartTime.getText().toString();
        activity.final_discharging_end_time = dischargingEndTime.getText().toString();
        activity.final_discharging_total_time = dischargingTotalTime.getText().toString();
        activity.final_discharging_remarks = dischargingRemarks.getText().toString();

        activity.final_physical_milage_start_time = physicalStartTime.getText().toString();
        activity.final_physical_milage_end_time = physicalEndTime.getText().toString();
        activity.final_physical_milage_total_time = physicalTotalTime.getText().toString();

        if(!TextUtils.isEmpty(startMilage.getText().toString()))
            activity.final_physical_milage_start = Double.parseDouble(startMilage.getText().toString());

        if(!TextUtils.isEmpty(endMilage.getText().toString()))
            activity.final_physical_milage_end = Double.parseDouble(endMilage.getText().toString());

        if(!TextUtils.isEmpty(totalMilage.getText().toString()))
            activity.final_physical_milage_total = Double.parseDouble(totalMilage.getText().toString());

        activity.final_physical_milage_remarks = physicalRemarks.getText().toString();

        activity.clickSaveBtn();
    }


    void setCalender(){
        Calendar calendar = Calendar.getInstance();
        CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(this)
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setPreselectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .setDoneText("Ok")
                .setCancelText("Cancel")
                .setThemeCustom(R.style.MyCustomBetterPickersDialogs);
        cdp.show(getActivity().getSupportFragmentManager(), "dateTag");
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

        String selectedYear = year+"";
        String selectedMonth = monthOfYear+1+"";
        String selectedDay = dayOfMonth+"";
        time = selectedYear+"-"+selectedMonth+"-"+selectedDay;

        RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(this)
                .setStartTime(10, 10)
                .setDoneText("Ok")
                .setCancelText("Cancel")
                .setThemeCustom(R.style.MyCustomBetterPickersDialogs);
        rtpd.show(getActivity().getSupportFragmentManager(), "timeTag");
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {

        time = time + " "+hourOfDay+":"+minute;

        if(isDischargePart) {

            if (isDischargeStartTimeClicked) {
                dischargingStartTime.setText(time);
                startTime = dischargingStartTime.getText().toString();
            } else {
                dischargingEndTime.setText(time);
                endTime = dischargingEndTime.getText().toString();
            }

            dischargingTotalTime.setText(getTotalTime(startTime, endTime));
        }
        else{

            if (isPhysicalStartTimeClicked) {
                physicalStartTime.setText(time);
                startTime = physicalStartTime.getText().toString();
            } else {
                physicalEndTime.setText(time);
                endTime = physicalEndTime.getText().toString();
            }

            physicalTotalTime.setText(getTotalTime(startTime, endTime));
        }

    }


    String getTotalTime(String startTime, String endTime){

        if(!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(endTime)){
            Date d1 = null;
            Date d2 = null;

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            try {
                d1 = format.parse(startTime);
                d2 = format.parse(endTime);

                //in milliseconds

                if(d1.getTime() > d2.getTime()){
                    dischargingTotalTime.setText("");
                    return "";
                }

                long diff = d2.getTime() - d1.getTime();

                long diffSeconds = diff / 1000 % 60;
                long diffMinutes = diff / (60 * 1000) % 60;
                long diffHours = diff / (60 * 60 * 1000);
                long diffDays = diff / (24 * 60 * 60 * 1000);

                System.out.print(diffDays + " days, ");
                System.out.print(diffHours + " hours, ");
                System.out.print(diffMinutes + " minutes, ");
                System.out.print(diffSeconds + " seconds.");

                return diffHours+":"+diffMinutes+":"+diffSeconds;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    void setPageSlide(){
        final TestActivity testActivity = (TestActivity)getActivity();
        testActivity.setPageSwipeEnable(false);
        workingStatusParentView.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
                //Toast.makeText(getActivity(), "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {
                Toast.makeText(getActivity(), "right", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeLeft() {
                //testActivity.setPageSwipeEnable(true);
                Toast.makeText(getActivity(), "All fields are required.", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeBottom() {
                //Toast.makeText(getActivity(), "bottom", Toast.LENGTH_SHORT).show();
            }

        });
    }


    private void setAllFields(){

        if(MyApplication.prefsHelper.get_working_status() == 1){
            workingStatusChecked(wsCheckYes);
        }
        else{
            workingStatusChecked(wsCheckNo);
        }

        if(MyApplication.prefsHelper.get_working_change_water() == 1){
            waterChangeChecked(waterChangeYes);
        }
        else{
            waterChangeChecked(waterChangeNo);
        }

        if(MyApplication.prefsHelper.get_working_battery_charged() == 1){
            batteryChangeChecked(batteryChangeYes);
        }
        else{
            batteryChangeChecked(batteryChangeNo);
        }

        if(MyApplication.prefsHelper.get_working_adjusted_cell_gravity() == 1){
            cellGravityAdjustChecked(cellGravityAdjustYes);
        }
        else{
            cellGravityAdjustChecked(cellGravityAdjustNo);
        }

        if(MyApplication.prefsHelper.get_working_adjusted_acid_level()== 1){
            acidLabelAdjustedChecked(acidLabelAdjustedYes);
        }
        else{
            acidLabelAdjustedChecked(acidLabelAdjustedNo);
        }

        ocv.setText(MyApplication.prefsHelper.get_final_ocv());
        loadVoltage.setText(MyApplication.prefsHelper.get_final_load_volt());
        spgr1.setText(MyApplication.prefsHelper.get_final_spgr1());
        spgr2.setText(MyApplication.prefsHelper.get_final_spgr2());
        spgr3.setText(MyApplication.prefsHelper.get_final_spgr3());
        spgr4.setText(MyApplication.prefsHelper.get_final_spgr4());
        spgr5.setText(MyApplication.prefsHelper.get_final_spgr5());
        spgr6.setText(MyApplication.prefsHelper.get_final_spgr6());
        remarks.setText(MyApplication.prefsHelper.get_final_remarks());

        batteryAh.setText(MyApplication.prefsHelper.get_final_discharging_battery_ah());
        ahDc.setText(MyApplication.prefsHelper.get_final_discharging_ah_dc());
        acWatt.setText(MyApplication.prefsHelper.get_final_discharging_ac_watt());
        dischargingStartTime.setText(MyApplication.prefsHelper.get_final_discharging_start_time());
        dischargingEndTime.setText(MyApplication.prefsHelper.get_final_discharging_end_time());
        dischargingTotalTime.setText(MyApplication.prefsHelper.get_final_discharging_total_time());
        dischargingRemarks.setText(MyApplication.prefsHelper.get_final_discharging_remarks());

        physicalStartTime.setText(MyApplication.prefsHelper.get_final_physical_milage_start_time());
        physicalEndTime.setText(MyApplication.prefsHelper.get_final_physical_milage_end_time());
        physicalTotalTime.setText(MyApplication.prefsHelper.get_final_physical_milage_total_time());
        startMilage.setText(MyApplication.prefsHelper.get_final_physical_milage_start());
        endMilage.setText(MyApplication.prefsHelper.get_final_physical_milage_end());
        totalMilage.setText(MyApplication.prefsHelper.get_final_physical_milage_total());
        physicalRemarks.setText(MyApplication.prefsHelper.get_final_physical_milage_remarks());
    }

}
