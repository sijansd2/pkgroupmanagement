package com.pkgroupmanagement.services.complainList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.pkgroupmanagement.R;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.services.createComplain.CreateComplainActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class InitialTestFragment extends Fragment
        implements CalendarDatePickerDialogFragment.OnDateSetListener, RadialTimePickerDialogFragment.OnTimeSetListener{

    @BindView(R.id.save)Button btnSave;
    @BindView(R.id.initialTestTitle)TextView initialTestTitle;
    @BindView(R.id.initialPageOptionalFieldText)TextView initialPageOptionalFieldText;
    @BindView(R.id.initialOptionalFieldLay)LinearLayout initialOptionalFieldLay;
    @BindView(R.id.initialOptionalFieldText)LinearLayout initialOptionalFieldText;

    @BindView(R.id.ocv)EditText ocv;
    @BindView(R.id.loadVoltage)EditText loadVoltage;
    @BindView(R.id.spgr1)EditText spgr1;
    @BindView(R.id.spgr2)EditText spgr2;
    @BindView(R.id.spgr3)EditText spgr3;
    @BindView(R.id.spgr4)EditText spgr4;
    @BindView(R.id.spgr5)EditText spgr5;
    @BindView(R.id.spgr6)EditText spgr6;
    @BindView(R.id.remarks)EditText remarks;

    @BindView(R.id.batteryAh)EditText batteryAh;
    @BindView(R.id.ahDc)EditText ahDc;
    @BindView(R.id.acWatt)EditText acWatt;
    @BindView(R.id.dischargingStartTimeLay)LinearLayout dischargingStartTimeLay;
    @BindView(R.id.dischargingEndTimeLay)LinearLayout dischargingEndTimeLay;
    @BindView(R.id.dischargingStartTime)TextView dischargingStartTime;
    @BindView(R.id.dischargingEndTime)TextView dischargingEndTime;
    @BindView(R.id.dischargingTotalTime)TextView dischargingTotalTime;
    @BindView(R.id.dischargingRemarks)EditText dischargingRemarks;

    @BindView(R.id.physicalStartTime)TextView physicalStartTime;
    @BindView(R.id.physicalEndTime)TextView physicalEndTime;
    @BindView(R.id.physicalTotalTime)TextView physicalTotalTime;
    @BindView(R.id.startMilage)EditText startMilage;
    @BindView(R.id.endMilage)EditText endMilage;
    @BindView(R.id.totalMilage)EditText totalMilage;
    @BindView(R.id.physicalRemarks)EditText physicalRemarks;

    Unbinder unbinder;

    TestActivity activity;
    String time;
    boolean isDischargeStartTimeClicked = false;
    boolean isPhysicalStartTimeClicked = false;
    boolean isDischargePart = false;
    boolean isPhysicalPart = false;
    String startTime, endTime, totalTime;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (TestActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.view_initial_test, container, false);
        unbinder = ButterKnife.bind(this, view);

        btnSave.setVisibility(View.VISIBLE);
        initialOptionalFieldText.setVisibility(View.VISIBLE);
        initialTestTitle.setText("Initial Test");

        if(!MyApplication.prefsHelper.get_is_new_test()){
            setAllFields();
        }

        return view;
    }

    @OnClick(R.id.initialPageOptionalFieldText)
    void initialPageOptionalFieldClicked(){

        if(initialOptionalFieldLay.getVisibility() == View.VISIBLE){
            initialOptionalFieldLay.setVisibility(View.GONE);
        }else {
            initialOptionalFieldLay.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.dischargingStartTimeLay)
    public void dischargeStartTimeClicked(){
        isDischargePart = true;
        isDischargeStartTimeClicked = true;
        setCalender();
    }

    @OnClick(R.id.dischargingEndTimeLay)
    public void dischargeEndTimeClicked(){
        isDischargePart = true;
        isDischargeStartTimeClicked = false;
        setCalender();
    }

    @OnClick(R.id.physicalStartTimeLay)
    public void physicalStartTimeLayClicked(){
        isDischargePart = false;
        isPhysicalStartTimeClicked = true;
        setCalender();
    }

    @OnClick(R.id.physicalEndTimeLay)
    public void physicalEndTimeLayClicked(){
        isDischargePart = false;
        isPhysicalStartTimeClicked = false;
        setCalender();
    }

    @OnClick(R.id.save)
    void saveClicked(){
        //Toast.makeText(getActivity(), "Not implemented yet", Toast.LENGTH_SHORT).show();

        if(!TextUtils.isEmpty(ocv.getText().toString()))
        activity.initial_ocv = Double.parseDouble(ocv.getText().toString());

        if(!TextUtils.isEmpty(loadVoltage.getText().toString()))
        activity.initial_load_volt = Double.parseDouble(loadVoltage.getText().toString());

        if(!TextUtils.isEmpty(spgr1.getText().toString()))
        activity.initial_spgr1 = Double.parseDouble(spgr1.getText().toString());

        if(!TextUtils.isEmpty(spgr2.getText().toString()))
        activity.initial_spgr2 = Double.parseDouble(spgr2.getText().toString());

        if(!TextUtils.isEmpty(spgr3.getText().toString()))
        activity.initial_spgr3 = Double.parseDouble(spgr3.getText().toString());

        if(!TextUtils.isEmpty(spgr4.getText().toString()))
        activity.initial_spgr4 = Double.parseDouble(spgr4.getText().toString());

        if(!TextUtils.isEmpty(spgr5.getText().toString()))
        activity.initial_spgr5 = Double.parseDouble(spgr5.getText().toString());

        if(!TextUtils.isEmpty(spgr6.getText().toString()))
        activity.initial_spgr6 = Double.parseDouble(spgr6.getText().toString());

        activity.initial_remarks = remarks.getText().toString();

        if(!TextUtils.isEmpty(batteryAh.getText().toString()))
        activity.initial_discharging_battery_ah = Double.parseDouble(batteryAh.getText().toString());

        if(!TextUtils.isEmpty(ahDc.getText().toString()))
        activity.initial_discharging_ah_dc = Double.parseDouble(ahDc.getText().toString());

        if(!TextUtils.isEmpty(acWatt.getText().toString()))
        activity.initial_discharging_ac_watt = Double.parseDouble(acWatt.getText().toString());

        activity.initial_discharging_start_time = dischargingStartTime.getText().toString();
        activity.initial_discharging_end_time = dischargingEndTime.getText().toString();
        activity.initial_discharging_total_time = dischargingTotalTime.getText().toString();
        activity.initial_discharging_remarks = dischargingRemarks.getText().toString();

        activity.initial_physical_milage_start_time = physicalStartTime.getText().toString();
        activity.initial_physical_milage_end_time = physicalEndTime.getText().toString();
        activity.initial_physical_milage_total_time = physicalTotalTime.getText().toString();

        if(!TextUtils.isEmpty(startMilage.getText().toString()))
        activity.initial_physical_milage_start = Double.parseDouble(startMilage.getText().toString());

        if(!TextUtils.isEmpty(endMilage.getText().toString()))
        activity.initial_physical_milage_end = Double.parseDouble(endMilage.getText().toString());

        if(!TextUtils.isEmpty(totalMilage.getText().toString()))
        activity.initial_physical_milage_total = Double.parseDouble(totalMilage.getText().toString());

        activity.initial_physical_milage_remarks = physicalRemarks.getText().toString();

        activity.clickSaveBtn();
    }


    void setCalender(){
        Calendar calendar = Calendar.getInstance();
        CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(this)
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setPreselectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .setDoneText("Ok")
                .setCancelText("Cancel")
                .setThemeCustom(R.style.MyCustomBetterPickersDialogs);
        cdp.show(getActivity().getSupportFragmentManager(), "dateTag");
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

        String selectedYear = year+"";
        String selectedMonth = monthOfYear+1+"";
        String selectedDay = dayOfMonth+"";
        time = selectedYear+"-"+selectedMonth+"-"+selectedDay;

        RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(this)
                .setStartTime(10, 10)
                .setDoneText("Ok")
                .setCancelText("Cancel")
                .setThemeCustom(R.style.MyCustomBetterPickersDialogs);
        rtpd.show(getActivity().getSupportFragmentManager(), "timeTag");
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {

        time = time + " "+hourOfDay+":"+minute;

        if(isDischargePart) {

            if (isDischargeStartTimeClicked) {
                dischargingStartTime.setText(time);
                startTime = dischargingStartTime.getText().toString();
            } else {
                dischargingEndTime.setText(time);
                endTime = dischargingEndTime.getText().toString();
            }

            dischargingTotalTime.setText(getTotalTime(startTime, endTime));
        }
        else{

            if (isPhysicalStartTimeClicked) {
                physicalStartTime.setText(time);
                startTime = physicalStartTime.getText().toString();
            } else {
                physicalEndTime.setText(time);
                endTime = physicalEndTime.getText().toString();
            }

            physicalTotalTime.setText(getTotalTime(startTime, endTime));
        }

    }


    String getTotalTime(String startTime, String endTime){

        if(!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(endTime)){
            Date d1 = null;
            Date d2 = null;

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            try {
                d1 = format.parse(startTime);
                d2 = format.parse(endTime);

                //in milliseconds

                if(d1.getTime() > d2.getTime()){
                    dischargingTotalTime.setText("");
                    return "";
                }

                long diff = d2.getTime() - d1.getTime();

                long diffSeconds = diff / 1000 % 60;
                long diffMinutes = diff / (60 * 1000) % 60;
                long diffHours = diff / (60 * 60 * 1000);
                long diffDays = diff / (24 * 60 * 60 * 1000);

                System.out.print(diffDays + " days, ");
                System.out.print(diffHours + " hours, ");
                System.out.print(diffMinutes + " minutes, ");
                System.out.print(diffSeconds + " seconds.");

                return diffHours+":"+diffMinutes+":"+diffSeconds;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(unbinder!=null)
        unbinder.unbind();
    }

    private void setAllFields(){

        ocv.setText(MyApplication.prefsHelper.get_initial_ocv());
        loadVoltage.setText(MyApplication.prefsHelper.get_initial_load_volt());
        spgr1.setText(MyApplication.prefsHelper.get_initial_spgr1());
        spgr2.setText(MyApplication.prefsHelper.get_initial_spgr2());
        spgr3.setText(MyApplication.prefsHelper.get_initial_spgr3());
        spgr4.setText(MyApplication.prefsHelper.get_initial_spgr4());
        spgr5.setText(MyApplication.prefsHelper.get_initial_spgr5());
        spgr6.setText(MyApplication.prefsHelper.get_initial_spgr6());
        remarks.setText(MyApplication.prefsHelper.get_initial_remarks());

        batteryAh.setText(MyApplication.prefsHelper.get_initial_discharging_battery_ah());
        ahDc.setText(MyApplication.prefsHelper.get_initial_discharging_ah_dc());
        acWatt.setText(MyApplication.prefsHelper.get_initial_discharging_ac_watt());
        dischargingStartTime.setText(MyApplication.prefsHelper.get_initial_discharging_start_time());
        dischargingEndTime.setText(MyApplication.prefsHelper.get_initial_discharging_end_time());
        dischargingTotalTime.setText(MyApplication.prefsHelper.get_initial_discharging_total_time());
        dischargingRemarks.setText(MyApplication.prefsHelper.get_initial_discharging_remarks());

        physicalStartTime.setText(MyApplication.prefsHelper.get_final_physical_milage_start_time());
        physicalEndTime.setText(MyApplication.prefsHelper.get_final_physical_milage_end_time());
        physicalTotalTime.setText(MyApplication.prefsHelper.get_final_physical_milage_total_time());
        startMilage.setText(MyApplication.prefsHelper.get_final_physical_milage_start());
        endMilage.setText(MyApplication.prefsHelper.get_final_physical_milage_end());
        totalMilage.setText(MyApplication.prefsHelper.get_final_physical_milage_total());
        physicalRemarks.setText(MyApplication.prefsHelper.get_final_physical_milage_remarks());
    }
}
