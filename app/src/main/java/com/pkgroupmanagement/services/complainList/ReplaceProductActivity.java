package com.pkgroupmanagement.services.complainList;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pkgroupmanagement.R;
import com.pkgroupmanagement.api.APIService;
import com.pkgroupmanagement.api.ApiUtils;
import com.pkgroupmanagement.api.response.ReplaceProductResponse;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.services.checkBattery.BatteryCheckActivity;
import com.pkgroupmanagement.services.createComplain.CreateComplainActivity;
import com.pkgroupmanagement.utils.UserPermission;
import com.pkgroupmanagement.utils.barCodeScanner.SimpleScannerActivity;
import com.pkgroupmanagement.utils.network.Connectivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ReplaceProductActivity extends AppCompatActivity {

    @BindView(R.id.etBarcode)EditText etBarcode;

    String service_id;

    public static Intent getStartIntent(Context context){
        return new Intent(context, ReplaceProductActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replace_product);

        UserPermission.getCameraPermission(this);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);

        TextView title = toolbar.findViewById(R.id.title);
        title.setText("Replacement Product");


        if(getIntent() != null){
            if(!TextUtils.isEmpty(getIntent().getStringExtra("id"))){
                service_id = getIntent().getStringExtra("id");
            }

        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        if(!TextUtils.isEmpty(CreateComplainActivity.pid))
            etBarcode.setText(CreateComplainActivity.pid);
    }

    @OnClick(R.id.barcodeScanner)
    void clickBarcodeScanner(){
        //Toast.makeText(this, "Not implemented yet", Toast.LENGTH_SHORT).show()
        Intent intent = new Intent(this,SimpleScannerActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnSave)
    public  void savedClicked(){
        if(Connectivity.isConnected(this)){
            if(!TextUtils.isEmpty(etBarcode.getText().toString())){
                saveReplacementProduct();
            }
            else{
                showSnackBar("Barcode field is empty.", findViewById(R.id.btnSave));
            }
        }
        else{
            showSnackBar(getResources().getString(R.string.check_internet), findViewById(R.id.btnSave));
        }
    }


    void saveReplacementProduct(){
        APIService apiService = new ApiUtils().getAPIService();
        apiService.saveReplacementProduct(Integer.valueOf(service_id), etBarcode.getText().toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<ReplaceProductResponse>() {
                    @Override
                    public void onNext(ReplaceProductResponse replaceProductResponse) {

                        if(replaceProductResponse.error != null){
                            Toast.makeText(ReplaceProductActivity.this, replaceProductResponse.error,Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(ReplaceProductActivity.this, replaceProductResponse.success,Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    void showSnackBar(String message, View v){

        View parentLayout = v;
        Snackbar snackbar = Snackbar.make(parentLayout,message,2000);
        View snackbarView = snackbar.getView();
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView mTextView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        mTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        snackbar.show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CreateComplainActivity.pid = null;
    }
}
