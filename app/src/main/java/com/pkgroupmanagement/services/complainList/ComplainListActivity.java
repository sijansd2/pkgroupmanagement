package com.pkgroupmanagement.services.complainList;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pkgroupmanagement.R;
import com.pkgroupmanagement.adapter.ComplainListAdapter;
import com.pkgroupmanagement.adapter.SearchComplainListAdapter;
import com.pkgroupmanagement.api.APIService;
import com.pkgroupmanagement.api.ApiUtils;
import com.pkgroupmanagement.api.response.ComplainListResponse;
import com.pkgroupmanagement.api.response.SearchComplainListResponse;
import com.pkgroupmanagement.interfaces.SelectedComplainItemListener;
import com.pkgroupmanagement.interfaces.SelectedItemListener;
import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.services.createComplain.SearchDealerActivity;
import com.pkgroupmanagement.utils.KeyboardControl;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

public class ComplainListActivity extends AppCompatActivity {

    @BindView(R.id.btnSearch)Button btnSearch;
    @BindView(R.id.btnCancel)Button btnCancel;
    @BindView(R.id.etBarcode)EditText etBarcode;
    @BindView(R.id.search)Button search;
    @BindView(R.id.searchView)LinearLayout searchView;
    @BindView(R.id.etDealerName)TextView etDealerName;
    @BindView(R.id.districtSpinner)SearchableSpinner districtSpinner;

    RecyclerView recyclerView;
    SearchComplainListAdapter adapter;
    ComplainListAdapter complainListAdapter;
    List<String> districtList;

    int dealer_id;
    String selectedDistrict;
    String barcode;

    private ProgressDialog pDialog;
    public void showProgressBar() {
        ProgressDialog();
        pDialog.show();
    }

    public void hideProgressBar() {
        if(pDialog != null) {
            pDialog.dismiss();
        }
    }
    public  void ProgressDialog()
    {
        if(pDialog == null){
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
        }

    }

    public static Intent getStartIntent(Context context){
        return new Intent(context, ComplainListActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complain_list);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);

        TextView title = toolbar.findViewById(R.id.title);
        title.setText("Complain List");

        ImageView imvClose = toolbar.findViewById(R.id.back);
        imvClose.setVisibility(View.VISIBLE);
        imvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.rvComplainList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){
                    selectedDistrict = null;
                    return;
                }
                selectedDistrict = districtList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        loadComplainList();

        if(MyApplication.prefsHelper.getDistricts() != null){
            String[] dist = MyApplication.prefsHelper.getDistricts().split(",");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dist);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            districtSpinner.setAdapter(adapter);
            districtList = Arrays.asList(dist);
        }
        else {
            loadDistrict();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(SearchDealerActivity.dealerItem != null && !TextUtils.isEmpty(SearchDealerActivity.dealerItem.name)){
            etDealerName.setText(SearchDealerActivity.dealerItem.name);
            dealer_id = SearchDealerActivity.dealerItem.id;
        }
    }

    @OnClick(R.id.etDealerName)
    void etDealerNameClicked(){
        startActivity(SearchDealerActivity.getStartIntent(ComplainListActivity.this));
    }

    @OnClick(R.id.search)
    public void searchClick(){
        searchView.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnCancel)
    public void cancelClick(){
        searchView.setVisibility(View.GONE);
        search.setVisibility(View.VISIBLE);
        KeyboardControl.hideKeyboard(this, btnCancel);
    }

    @OnClick(R.id.btnSearch)
    public void btnSearchClick(){
        KeyboardControl.hideKeyboard(this, btnSearch);
        loadSearchedComplainList();
    }


    public void itemClick(View view){
        //Toast.makeText(this, "item clicked", Toast.LENGTH_SHORT).show();
        //showDialog(this);
    }

//    public void continueTestClick(View view){
//        //Toast.makeText(this, "item clicked", Toast.LENGTH_SHORT).show();
//        Intent intent = TestActivity.getStartIntent(this);
//        startActivity(intent);
//    }

    private void loadComplainList(){

        showProgressBar();

        APIService apiService = new ApiUtils().getAPIService();
        apiService.getComplainList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<ComplainListResponse>() {
                    @Override
                    public void onNext(ComplainListResponse complainListResponse) {

                        complainListAdapter = new ComplainListAdapter(complainListResponse.complainList, selectedComplainItemListener);
                        recyclerView.setAdapter(complainListAdapter);
                        hideProgressBar();
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        hideProgressBar();
                    }

                    @Override
                    public void onComplete() {

                        hideProgressBar();
                    }
                });
    }

    private void loadSearchedComplainList(){

        showProgressBar();
        barcode = etBarcode.getText().toString();
        APIService apiService = new ApiUtils().getAPIService();
        apiService.getComplainList(dealer_id+"", barcode, selectedDistrict)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<SearchComplainListResponse>() {
                    @Override
                    public void onNext(SearchComplainListResponse complainListResponse) {

                        adapter = new SearchComplainListAdapter(complainListResponse.complainList, selectedItem);
                        recyclerView.setAdapter(adapter);
                        hideProgressBar();
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        hideProgressBar();
                    }

                    @Override
                    public void onComplete() {
                        hideProgressBar();
                    }
                });
    }


    private void loadDistrict(){

        APIService apiService = new ApiUtils().getAPIService();
        apiService.getDistritList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSubscriber<List<String>>() {
                    @Override
                    public void onNext(List<String> strings) {
                        //Log.e("aaaDistrict", strings.toString());
                        setDistrictList(strings);
                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    private void setDistrictList(List<String> districtList) {
        this.districtList = districtList;
        StringBuilder sb = new StringBuilder();
        for (int i=0; i< districtList.size(); i++){
            sb.append(districtList.get(i)).append(",");
        }

        MyApplication.prefsHelper.setDistricts(sb.toString());

        if(districtList == null)return;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, districtList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        districtSpinner.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SearchDealerActivity.dealerItem = null;
    }

    private void showDetailDialog(Activity activity, SearchComplainListResponse.ComplainList item){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.item_complain_details);


        TextView tvName = dialog.findViewById(R.id.name);
        tvName.setText(item.dealer_name);

        TextView phone = dialog.findViewById(R.id.phone);
        phone.setText(item.dealer_phone);

        TextView district = dialog.findViewById(R.id.district);
        district.setText(item.dealer_district);

        TextView assignment = dialog.findViewById(R.id.assignment);
        assignment.setText("SRO: ("+item.sro+") \nService Exec: ("+item.service_exec+")");

        TextView location = dialog.findViewById(R.id.location);
        location.setText(item.service_location);

        TextView complain = dialog.findViewById(R.id.complain);
        complain.setText(item.customer_claim);

        TextView history = dialog.findViewById(R.id.history);
        history.setText("Production Date "+item.date_production+" Delivery Date "+item.date_delivered);

        TextView product = dialog.findViewById(R.id.product);
        product.setText(item.st_product_name);

        TextView decision = dialog.findViewById(R.id.decision);
        decision.setText(item.st_decision);

        TextView lastupdate = dialog.findViewById(R.id.lastupdate);
        lastupdate.setText(item.st_last_update);

        TextView testStatus = dialog.findViewById(R.id.testStatus);
        testStatus.setText(item.st_test_step);

        TextView barcode = dialog.findViewById(R.id.barcode);
        barcode.setText(item.pid);

        TextView service_exec = dialog.findViewById(R.id.service_exec);
        service_exec.setText(item.service_exec);

        TextView salesRep = dialog.findViewById(R.id.salesRep);
        salesRep.setText(item.sro);

        TextView date = dialog.findViewById(R.id.date);
        date.setText(item.created_at);

        dialog.show();
    }


    private void showDetailDialog2(Activity activity, ComplainListResponse.ComplainList item){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.item_complain_details);


        TextView tvName = dialog.findViewById(R.id.name);
        tvName.setText(item.dealer_name);

        TextView phone = dialog.findViewById(R.id.phone);
        phone.setText(item.dealer_phone);

        TextView district = dialog.findViewById(R.id.district);
        district.setText(item.dealer_district);

        TextView assignment = dialog.findViewById(R.id.assignment);
        assignment.setText("SRO: ("+item.sro+") \nService Exec: ("+item.service_exec+")");

        TextView location = dialog.findViewById(R.id.location);
        location.setText(item.service_location);

        TextView complain = dialog.findViewById(R.id.complain);
        complain.setText(item.customer_claim);

        TextView history = dialog.findViewById(R.id.history);
        history.setText("Production Date "+item.date_production+" Delivery Date "+item.date_delivered);

        TextView product = dialog.findViewById(R.id.product);
        product.setText(item.st_product_name);

        TextView decision = dialog.findViewById(R.id.decision);
        decision.setText(item.st_decision);

        TextView lastupdate = dialog.findViewById(R.id.lastupdate);
        lastupdate.setText(item.st_last_update);

        TextView testStatus = dialog.findViewById(R.id.testStatus);
        testStatus.setText(item.st_test_step);

        TextView barcode = dialog.findViewById(R.id.barcode);
        barcode.setText(item.pid);

        TextView service_exec = dialog.findViewById(R.id.service_exec);
        service_exec.setText(item.service_exec);

        TextView salesRep = dialog.findViewById(R.id.salesRep);
        salesRep.setText(item.sro);

        TextView date = dialog.findViewById(R.id.date);
        date.setText(item.created_at);

        dialog.show();
    }

    SelectedItemListener selectedItem = new SelectedItemListener() {
        @Override
        public void onItemClicked(SearchComplainListResponse.ComplainList item) {
            showDetailDialog(ComplainListActivity.this, item);
        }
    };

    SelectedComplainItemListener selectedComplainItemListener = new SelectedComplainItemListener() {
        @Override
        public void onItemClicked(ComplainListResponse.ComplainList item) {
            showDetailDialog2(ComplainListActivity.this, item);
        }
    };
}
