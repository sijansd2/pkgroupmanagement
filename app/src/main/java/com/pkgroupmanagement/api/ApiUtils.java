package com.pkgroupmanagement.api;

public class ApiUtils {

    public ApiUtils() {}

    public  APIService getAPIService() {

        return new RetrofitClient().getClient("https://api.fusionbangladesh.com/").create(APIService.class);
    }
}