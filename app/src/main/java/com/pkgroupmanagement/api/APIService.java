package com.pkgroupmanagement.api;

import com.pkgroupmanagement.api.response.ComplainListResponse;
import com.pkgroupmanagement.api.response.ReplaceProductResponse;
import com.pkgroupmanagement.api.response.SearchComplainListResponse;
import com.pkgroupmanagement.api.response.CreateComplainResponse;
import com.pkgroupmanagement.api.response.DealerListResponse;
import com.pkgroupmanagement.api.response.TestStatusGetResponse;
import com.pkgroupmanagement.api.response.TestStatusSaveResponse;
import com.pkgroupmanagement.api.response.VersionCheckResponse;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIService {

    @GET("login/app-version")
    Flowable<VersionCheckResponse> checkVersion();

    @GET("v3/my-dealers/")
    Flowable<List<DealerListResponse>> dealerListForComplainCreate();

    @GET("v3/find-dealers")
    Flowable<List<DealerListResponse>> searchDealer(@Query("q") String q);

    @GET("service/districts")
    Flowable<List<String>> getDistritList();

    @GET("service/pending")
    Flowable<ComplainListResponse> getComplainList();

    @GET("service/pending")
    Flowable<SearchComplainListResponse> getComplainList(
            @Query("dealer_id") String dealer_id,
            @Query("barcode") String barcode,
            @Query("district") String district);

    @FormUrlEncoded
    @POST("service/create")
    Flowable<CreateComplainResponse> createComplain(
            @Field("dealer_id") String dealer_id,
            @Field("pid") String pid,
            @Field("service_location") String service_location,
            @Field("customer_claim") String customer_claim,
            @Field("date_production") String date_production);


    @GET("service-test/view")
    Flowable<TestStatusGetResponse> getTestStatus(@Query("id") String id);


    @FormUrlEncoded
    @POST("service-test/replace")
    Flowable<ReplaceProductResponse> saveReplacementProduct(@Field("service_id") int id, @Field("replaced_pid") String replaced_pid);


    @FormUrlEncoded
    @POST("service-test/test")
    Flowable<TestStatusSaveResponse> makeTest(

            @Field("id") int id,
            @Field("service_id") int service_id,
            @Field("pid") String pid,
            @Field("replaced_pid") String replaced_pid,

            @Field("initial_ocv") double initial_ocv,
            @Field("initial_spgr1") double initial_spgr1,
            @Field("initial_spgr2") double initial_spgr2,
            @Field("initial_spgr3") double initial_spgr3,
            @Field("initial_spgr4") double initial_spgr4,
            @Field("initial_spgr5") double initial_spgr5,
            @Field("initial_spgr6") double initial_spgr6,
            @Field("initial_load_volt") double initial_load_volt,
            @Field("initial_remarks") String initial_remarks,

            @Field("physical_condition") String physical_condition,
            @Field("physical_plate_condition") String physical_plate_condition,
            @Field("physical_sulfation") String physical_sulfation,
            @Field("physical_battery_acid") String physical_battery_acid,

            @Field("initial_discharging_battery_ah") double initial_discharging_battery_ah,
            @Field("initial_discharging_ah_dc") double initial_discharging_ah_dc,
            @Field("initial_discharging_ac_watt") double initial_discharging_ac_watt,
            @Field("initial_discharging_start_time") String initial_discharging_start_time,
            @Field("initial_discharging_end_time") String initial_discharging_end_time,
            @Field("initial_discharging_total_time") String initial_discharging_total_time,
            @Field("initial_discharging_remarks") String initial_discharging_remarks,

            @Field("initial_physical_milage_start_time") String initial_physical_milage_start_time,
            @Field("initial_physical_milage_end_time") String initial_physical_milage_end_time,
            @Field("initial_physical_milage_total_time") String initial_physical_milage_total_time,
            @Field("initial_physical_milage_start") double  initial_physical_milage_start,
            @Field("initial_physical_milage_end") double  initial_physical_milage_end,
            @Field("initial_physical_milage_total") double  initial_physical_milage_total,
            @Field("initial_physical_milage_remarks") String  initial_physical_milage_remarks,

            @Field("working_status") int  working_status,
            @Field("working_change_water") int  working_change_water,
            @Field("working_battery_charged") int  working_battery_charged,
            @Field("working_adjusted_cell_gravity") int  working_adjusted_cell_gravity,
            @Field("working_adjusted_acid_level") int  working_adjusted_acid_level,

            @Field("final_ocv") double final_ocv,
            @Field("final_spgr1") double final_spgr1,
            @Field("final_spgr2") double final_spgr2,
            @Field("final_spgr3") double final_spgr3,
            @Field("final_spgr4") double final_spgr4,
            @Field("final_spgr5") double final_spgr5,
            @Field("final_spgr6") double final_spgr6,
            @Field("final_load_volt") double final_load_volt,
            @Field("final_remarks") String final_remarks,

            @Field("final_discharging_battery_ah") double final_discharging_battery_ah,
            @Field("final_discharging_ah_dc") double final_discharging_ah_dc,
            @Field("final_discharging_ac_watt") double final_discharging_ac_watt,
            @Field("final_discharging_start_time") String final_discharging_start_time,
            @Field("final_discharging_end_time") String final_discharging_end_time,
            @Field("final_discharging_total_time") String final_discharging_total_time,
            @Field("final_discharging_remarks") String final_discharging_remarks,

            @Field("final_physical_milage_start_time") String final_physical_milage_start_time,
            @Field("final_physical_milage_end_time") String final_physical_milage_end_time,
            @Field("final_physical_milage_total_time") String final_physical_milage_total_time,
            @Field("final_physical_milage_start") double  final_physical_milage_start,
            @Field("final_physical_milage_end") double  final_physical_milage_end,
            @Field("final_physical_milage_total") double  final_physical_milage_total,
            @Field("final_physical_milage_remarks") String  final_physical_milage_remarks,

            @Field("decision") String decision,
            @Field("decision_comments") String decision_comments,
            @Field("test_step") int test_step
    );


}
