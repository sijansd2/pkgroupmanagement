package com.pkgroupmanagement.api.response;

import com.google.gson.annotations.SerializedName;

public class VersionCheckResponse {

    @SerializedName("app_version")
    public String app_version;
    @SerializedName("message")
    public String message;


    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
