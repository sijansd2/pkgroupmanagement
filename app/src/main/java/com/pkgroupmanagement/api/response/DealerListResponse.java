package com.pkgroupmanagement.api.response;

import com.google.gson.annotations.SerializedName;

public class DealerListResponse {


    @SerializedName("id")
    public int id;
    @SerializedName("name")
    public String name;
    @SerializedName("district")
    public String district;
    @SerializedName("sro_id")
    public int sro_id;
    @SerializedName("service_exec_id")
    public String service_exec_id;
    @SerializedName("registered")
    public int registered;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public int getSro_id() {
        return sro_id;
    }

    public void setSro_id(int sro_id) {
        this.sro_id = sro_id;
    }

    public String getService_exec_id() {
        return service_exec_id;
    }

    public void setService_exec_id(String service_exec_id) {
        this.service_exec_id = service_exec_id;
    }

    public int getRegistered() {
        return registered;
    }

    public void setRegistered(int registered) {
        this.registered = registered;
    }
}
