package com.pkgroupmanagement.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ComplainListResponse {


    @SerializedName("reqParams")
    public List<ReqParams> reqParams;
    @SerializedName("complainList")
    public List<ComplainList> complainList;

    public static class ReqParams {
    }

    public static class ComplainList {
        @SerializedName("id")
        public String id;
        @SerializedName("pid")
        public String pid;
        @SerializedName("service_location")
        public String service_location;
        @SerializedName("customer_claim")
        public String customer_claim;
        @SerializedName("date_production")
        public String date_production;
        @SerializedName("date_delivered")
        public String date_delivered;
        @SerializedName("status")
        public String status;
        @SerializedName("updated_by")
        public String updated_by;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
        @SerializedName("dealer_name")
        public String dealer_name;
        @SerializedName("dealer_phone")
        public String dealer_phone;
        @SerializedName("dealer_district")
        public String dealer_district;
        @SerializedName("sro")
        public String sro;
        @SerializedName("sro_id")
        public String sro_id;
        @SerializedName("service_exec")
        public String service_exec;
        @SerializedName("se_id")
        public String se_id;
        @SerializedName("is_new")
        public String is_new;
        @SerializedName("st_product_name")
        public String st_product_name;
        @SerializedName("st_decision")
        public String st_decision;
        @SerializedName("st_last_update")
        public String st_last_update;
        @SerializedName("st_test_step")
        public String st_test_step;
    }
}
