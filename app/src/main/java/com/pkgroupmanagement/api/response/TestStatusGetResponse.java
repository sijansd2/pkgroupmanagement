package com.pkgroupmanagement.api.response;

import com.google.gson.annotations.SerializedName;

public class TestStatusGetResponse {

    @SerializedName("error")
    public String error;

    @SerializedName("service_id")
    public int service_id;
    @SerializedName("product_name")
    public String product_name;
    @SerializedName("pid")
    public String pid;
    @SerializedName("replaced_pid")
    public String replaced_pid;
    @SerializedName("initial_ocv")
    public String initial_ocv;
    @SerializedName("initial_spgr1")
    public String initial_spgr1;
    @SerializedName("initial_spgr2")
    public String initial_spgr2;
    @SerializedName("initial_spgr3")
    public String initial_spgr3;
    @SerializedName("initial_spgr4")
    public String initial_spgr4;
    @SerializedName("initial_spgr5")
    public String initial_spgr5;
    @SerializedName("initial_spgr6")
    public String initial_spgr6;
    @SerializedName("initial_load_volt")
    public String initial_load_volt;
    @SerializedName("initial_remarks")
    public String initial_remarks;
    @SerializedName("physical_condition")
    public String physical_condition;
    @SerializedName("physical_plate_condition")
    public String physical_plate_condition;
    @SerializedName("physical_sulfation")
    public String physical_sulfation;
    @SerializedName("physical_battery_acid")
    public String physical_battery_acid;
    @SerializedName("initial_discharging_battery_ah")
    public String initial_discharging_battery_ah;
    @SerializedName("initial_discharging_ah_dc")
    public String initial_discharging_ah_dc;
    @SerializedName("initial_discharging_ac_watt")
    public String initial_discharging_ac_watt;
    @SerializedName("initial_discharging_start_time")
    public String initial_discharging_start_time;
    @SerializedName("initial_discharging_end_time")
    public String initial_discharging_end_time;
    @SerializedName("initial_discharging_total_time")
    public String initial_discharging_total_time;
    @SerializedName("initial_discharging_remarks")
    public String initial_discharging_remarks;
    @SerializedName("initial_physical_milage_start_time")
    public String initial_physical_milage_start_time;
    @SerializedName("initial_physical_milage_end_time")
    public String initial_physical_milage_end_time;
    @SerializedName("initial_physical_milage_total_time")
    public String initial_physical_milage_total_time;
    @SerializedName("initial_physical_milage_start")
    public String initial_physical_milage_start;
    @SerializedName("initial_physical_milage_end")
    public String initial_physical_milage_end;
    @SerializedName("initial_physical_milage_total")
    public String initial_physical_milage_total;
    @SerializedName("initial_physical_milage_remarks")
    public String initial_physical_milage_remarks;
    @SerializedName("working_status")
    public int working_status;
    @SerializedName("working_change_water")
    public int working_change_water;
    @SerializedName("working_battery_charged")
    public int working_battery_charged;
    @SerializedName("working_adjusted_cell_gravity")
    public int working_adjusted_cell_gravity;
    @SerializedName("working_adjusted_acid_level")
    public int working_adjusted_acid_level;
    @SerializedName("final_ocv")
    public String final_ocv;
    @SerializedName("final_spgr1")
    public String final_spgr1;
    @SerializedName("final_spgr2")
    public String final_spgr2;
    @SerializedName("final_spgr3")
    public String final_spgr3;
    @SerializedName("final_spgr4")
    public String final_spgr4;
    @SerializedName("final_spgr5")
    public String final_spgr5;
    @SerializedName("final_spgr6")
    public String final_spgr6;
    @SerializedName("final_load_volt")
    public String final_load_volt;
    @SerializedName("final_remarks")
    public String final_remarks;
    @SerializedName("final_discharging_battery_ah")
    public String final_discharging_battery_ah;
    @SerializedName("final_discharging_ah_dc")
    public String final_discharging_ah_dc;
    @SerializedName("final_discharging_ac_watt")
    public String final_discharging_ac_watt;
    @SerializedName("final_discharging_start_time")
    public String final_discharging_start_time;
    @SerializedName("final_discharging_end_time")
    public String final_discharging_end_time;
    @SerializedName("final_discharging_total_time")
    public String final_discharging_total_time;
    @SerializedName("final_discharging_remarks")
    public String final_discharging_remarks;
    @SerializedName("final_physical_milage_start_time")
    public String final_physical_milage_start_time;
    @SerializedName("final_physical_milage_end_time")
    public String final_physical_milage_end_time;
    @SerializedName("final_physical_milage_total_time")
    public String final_physical_milage_total_time;
    @SerializedName("final_physical_milage_start")
    public String final_physical_milage_start;
    @SerializedName("final_physical_milage_end")
    public String final_physical_milage_end;
    @SerializedName("final_physical_milage_total")
    public String final_physical_milage_total;
    @SerializedName("final_physical_milage_remarks")
    public String final_physical_milage_remarks;
    @SerializedName("decision")
    public String decision;
    @SerializedName("decision_comments")
    public String decision_comments;
    @SerializedName("test_step")
    public int test_step;
    @SerializedName("updated_at")
    public String updated_at;
    @SerializedName("updated_by")
    public int updated_by;
    @SerializedName("finished")
    public int finished;
}
