package com.pkgroupmanagement.api.response;

import com.google.gson.annotations.SerializedName;

public class CreateComplainResponse {

    @SerializedName("success")
    public String success;
    @SerializedName("error")
    public String errors;
}
