package com.pkgroupmanagement.api.response;

import com.google.gson.annotations.SerializedName;

public class ReplaceProductResponse {


    @SerializedName("success")
    public String success;
    @SerializedName("error")
    public String error;
}
