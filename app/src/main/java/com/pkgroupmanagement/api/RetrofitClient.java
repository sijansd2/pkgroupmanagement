package com.pkgroupmanagement.api;


import com.pkgroupmanagement.main.MyApplication;
import com.pkgroupmanagement.utils.network.MyOkHttpClient;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient{

private Retrofit retrofit2=null;

public  Retrofit getClient(String baseUrl){

                if(retrofit2==null){
                        OkHttpClient client = MyOkHttpClient.getNewHttpClient(MyApplication.prefsHelper.getApiToken());

                        retrofit2=new Retrofit.Builder()
                                .baseUrl(baseUrl)
                                .addConverterFactory(GsonConverterFactory.create())
                                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                                .client(client)
                                .build();
                }
                return retrofit2;
        }
}
