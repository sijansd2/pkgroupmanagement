package com.pkgroupmanagement.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

/**
 * Created by admin on 7/11/17.
 */

public class UserPermission {

    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    public static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 2;
    public static final int MY_PERMISSIONS_REQUEST_location = 6;
    public static final int MY_PERMISSIONS_CALL_PHONE = 3;
    public static final int MY_PERMISSIONS_SMS_SEND = 4;
    public static final int MY_PERMISSIONS_SMS_RECEIVE = 5;
    public static final int MY_PERMISSIONS_CAMERA = 7;

    public static void getPermissions(Activity activity){

        int PERMISSION_ALL = 100;
        String[] PERMISSIONS = {Manifest.permission.READ_CONTACTS,
                Manifest.permission.RECORD_AUDIO,Manifest.permission.READ_PHONE_STATE,Manifest.permission.RECEIVE_SMS};

        if (!hasPermissions(activity, PERMISSIONS)) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, PERMISSION_ALL);
        }
    }


    public static void getLocationPermission(Activity activity){

        String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION};

        if (!hasPermissions(activity, PERMISSIONS)) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, MY_PERMISSIONS_REQUEST_location);
        }
    }

    public static void getCameraPermission(Activity activity){

        String[] PERMISSIONS = {Manifest.permission.CAMERA};

        if (!hasPermissions(activity, PERMISSIONS)) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, MY_PERMISSIONS_CAMERA);
        }
    }

    public static void getAudioPermission(Activity activity){

        String[] PERMISSIONS = {Manifest.permission.RECORD_AUDIO};

        if (!hasPermissions(activity, PERMISSIONS)) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
        }
    }

    public static void getContactPermission(Activity activity){

        String[] PERMISSIONS = {Manifest.permission.READ_CONTACTS};

        if (!hasPermissions(activity, PERMISSIONS)) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void getCallPermission(Activity activity){

        String[] PERMISSIONS = {Manifest.permission.CALL_PHONE};

        if (!hasPermissions(activity, PERMISSIONS)) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, MY_PERMISSIONS_CALL_PHONE);
        }
    }

    public static void getSendSmsPermission(Activity activity){

        String[] PERMISSIONS = {Manifest.permission.SEND_SMS};

        if (!hasPermissions(activity, PERMISSIONS)) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, MY_PERMISSIONS_SMS_SEND);
        }
    }

    public static void getReceiveSmsPermission(Activity activity){

        String[] PERMISSIONS = {Manifest.permission.RECEIVE_SMS};

        if (!hasPermissions(activity, PERMISSIONS)) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, MY_PERMISSIONS_SMS_RECEIVE);
        }
    }
}
