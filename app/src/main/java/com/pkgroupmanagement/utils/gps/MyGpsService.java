package com.pkgroupmanagement.utils.gps;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;

import com.pkgroupmanagement.main.MainActivity;

/**
 * Created by macbookpro on 12/16/17.
 */

public class MyGpsService extends Service {


    public static final String TAG = "PkTag----";
    public static LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000 * 20;
    private static final float LOCATION_DISTANCE = 1f;
    public static boolean isGPSTrackingEnabled = false;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.e(TAG, "ServiceStart: "+ "Successfully");
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        initializeLocationManager();
        startTrack();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "ServiceDestroy: "+ "Successfully");
        stopTrack();
    }



    public static MyGpsService.LocationListener[] mLocationListeners = new MyGpsService.LocationListener[] {
            new MyGpsService.LocationListener(LocationManager.GPS_PROVIDER),
            new MyGpsService.LocationListener(LocationManager.NETWORK_PROVIDER)
    };




    public static class LocationListener implements android.location.LocationListener
    {
        Location mLastLocation;

        public LocationListener(String provider)
        {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location)
        {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
            Log.i("Location:",location+"");
            Double[] latLong = {location.getLatitude(),location.getLongitude()};
            Message msg = Message.obtain(MainActivity.handler,1, latLong);
            msg.sendToTarget();
            //stopTrack();
        }

        @Override
        public void onProviderDisabled(String provider)
        {
            Log.e(TAG, "onProviderDisabled: " + provider);
            isGPSTrackingEnabled = false;
        }

        @Override
        public void onProviderEnabled(String provider)
        {
            Log.e(TAG, "onProviderEnabled: " + provider);
            isGPSTrackingEnabled = true;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }




    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            startTrack();
        }
    }

    public static void startTrack(){

        if(mLocationManager == null)return;

        Log.e(TAG, "onCreate");
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    public static void stopTrack(){

        Log.e(TAG, "Stop Track");
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    public boolean getIsGPSTrackingEnabled() {

        return this.isGPSTrackingEnabled;
    }



}
