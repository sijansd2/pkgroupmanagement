package com.pkgroupmanagement.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by admin on 5/24/17.
 */

public class KeyboardControl {

    public void showKeyboard(){

    }

    public static void hideKeyboard(Context c, View v){
        if (v != null) {
            InputMethodManager inputMethodManager = (InputMethodManager)  c.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }
}
