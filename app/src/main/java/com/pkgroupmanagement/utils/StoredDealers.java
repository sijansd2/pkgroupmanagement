package com.pkgroupmanagement.utils;

import java.io.Serializable;

/**
 * Created by macbookpro on 12/16/17.
 */

public class StoredDealers implements Serializable{

    String id = "";
    String name = "";
    String phone = "";
    String address = "";
    String district = "";
    String thana = "";
    String latitude = "";
    String longitude = "";
    String selling_brands = "";
    String selling_cap = "";
    String percent_of_ev = "";
    String payment_system = "";
    String registered = "";
    String sro_id = "";
    String sro_visit_date = "";
    String sro_progress = "";
    String updated_at = "";
    String updated_by = "";
    String photo = "";
    String time = "";
    String isExistingDealer = "";
    String visitedPreviously = "";
    String informationChange = "";
    String comments = "";

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }


    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIsExistingDealer() {
        return isExistingDealer;
    }

    public void setIsExistingDealer(String isExistingDealer) {
        this.isExistingDealer = isExistingDealer;
    }

    public String getVisitedPreviously() {
        return visitedPreviously;
    }

    public void setVisitedPreviously(String visitedPreviously) {
        this.visitedPreviously = visitedPreviously;
    }

    public String getInformationChange() {
        return informationChange;
    }

    public void setInformationChange(String informationChange) {
        this.informationChange = informationChange;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getThana() {
        return thana;
    }

    public void setThana(String thana) {
        this.thana = thana;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSelling_brands() {
        return selling_brands;
    }

    public void setSelling_brands(String selling_brands) {
        this.selling_brands = selling_brands;
    }

    public String getSelling_cap() {
        return selling_cap;
    }

    public void setSelling_cap(String selling_cap) {
        this.selling_cap = selling_cap;
    }

    public String getPercent_of_ev() {
        return percent_of_ev;
    }

    public void setPercent_of_ev(String percent_of_ev) {
        this.percent_of_ev = percent_of_ev;
    }

    public String getPayment_system() {
        return payment_system;
    }

    public void setPayment_system(String payment_system) {
        this.payment_system = payment_system;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public String getSro_id() {
        return sro_id;
    }

    public void setSro_id(String sro_id) {
        this.sro_id = sro_id;
    }

    public String getSro_visit_date() {
        return sro_visit_date;
    }

    public void setSro_visit_date(String sro_visit_date) {
        this.sro_visit_date = sro_visit_date;
    }

    public String getSro_progress() {
        return sro_progress;
    }

    public void setSro_progress(String sro_progress) {
        this.sro_progress = sro_progress;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
