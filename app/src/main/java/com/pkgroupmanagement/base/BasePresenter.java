package com.pkgroupmanagement.base;

import com.pkgroupmanagement.model.DataManager;

/**
 * Created by macbookpro on 12/9/17.
 */

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {


    DataManager mDataManager;
    private V mMvpView;

    public BasePresenter(DataManager dataManager){

        this.mDataManager = dataManager;
    }

    @Override
    public void onAttach(V mvpView) {
        this.mMvpView = mvpView;
    }


    public V getMvpView(){
        return this.mMvpView;
    }

    public DataManager getDataManager(){
        return this.mDataManager;
    }
}
