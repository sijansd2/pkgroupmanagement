package com.pkgroupmanagement.base;

/**
 * Created by macbookpro on 12/9/17.
 */

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);
}
